﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(LevelSelectDoor), true)]
public class LevelSelectDoorEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        var picker = target as LevelSelectDoor;
        var oldScene = AssetDatabase.LoadAssetAtPath<SceneAsset>(picker.levelScene);

        serializedObject.Update();

        EditorGUI.BeginChangeCheck();
        var newScene = EditorGUILayout.ObjectField("Level Scene", oldScene, typeof(SceneAsset), false) as SceneAsset;

        if (EditorGUI.EndChangeCheck())
        {
            var newPath = AssetDatabase.GetAssetPath(newScene);
            var levelSceneProperty = serializedObject.FindProperty("levelScene");
            levelSceneProperty.stringValue = newPath;
        }

        serializedObject.ApplyModifiedProperties();
    }
}
