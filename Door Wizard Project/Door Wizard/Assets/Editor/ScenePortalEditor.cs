﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ScenePortal), true)]
public class ScenePortalEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        var picker = target as ScenePortal;
        var oldScene = AssetDatabase.LoadAssetAtPath<SceneAsset>(picker.nextLevel);

        serializedObject.Update();

        EditorGUI.BeginChangeCheck();
        var newScene = EditorGUILayout.ObjectField("Next Level", oldScene, typeof(SceneAsset), false) as SceneAsset;

        if (EditorGUI.EndChangeCheck())
        {
            var newPath = AssetDatabase.GetAssetPath(newScene);
            var levelSceneProperty = serializedObject.FindProperty("nextLevel");
            levelSceneProperty.stringValue = newPath;
        }

        serializedObject.ApplyModifiedProperties();
    }
}
