﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelSelectDoor : ClassicDoor
{
    [Header("Level Select Door")]
    public GameObject lockSprite = null;
   // public GameObject unLockSprite = null;
    public int starsNeededInt;
    [HideInInspector] public string levelScene = null;

    public bool isMagicSchoolDoor = false;
    bool doLoadScene = true;

    protected override void OnEnable()
    {
        base.OnEnable();
        
        if (GameManager.Instance.StarsCollected >= starsNeededInt)
        {
            lockSprite.SetActive(false);
            isLocked = false;
        }
        else
        {
            lockSprite.SetActive(true);
            isLocked = true;
        }
    }

    public override void Update()
    {
        base.Update();
        StarsNeededReached();
    }


    void StarsNeededReached()
    {
        if (GameManager.Instance.StarsCollected >= starsNeededInt)
        {
            lockSprite.SetActive(false);
            isLocked = false;
        }
        else
        {
            lockSprite.SetActive(true);
            isLocked = true;
        }
    }

    protected void OnCollisionEnter(Collision collision)
    {
        if (doLoadScene)
        {
            if (collision.gameObject.layer == LayerManager.Instance.PlayerLayerInt)
            {
                EventManager.TriggerEvent(Events.PreLoadLevel);
                UIManager.Instance.LoadScene(levelScene);

                doLoadScene = false;
            }
        }
    }

    protected override void OnRegisterObjects()
    {
        LevelSelectManager.Instance.RegisterLevelSelectDoor(this);
    }
}
