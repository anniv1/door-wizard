﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneWayPortal : OpenCloseDoor
{
    [Header("One Door Portal")]
    [SerializeField] protected Vector3 rotAngle = new Vector3(0, -90, 0);
    public Collider portalCollider = null;
    public Transform exitSpawnPos = null;

    void Start()
    {
        portalCollider.enabled = false;
    }

    public override void Open(GameObject door)
    {
        base.Open(door);
        door.transform.localRotation = Quaternion.Euler(-rotAngle);
        portalCollider.enabled = true;
    }

    public override void Close(GameObject door)
    {
        base.Close(door);
        door.transform.localRotation = Quaternion.Euler(Vector3.zero);
        portalCollider.enabled = false;
    }

    protected void OnCollisionEnter(Collision collision)
    {
        collision.gameObject.transform.position = exitSpawnPos.position;
    }
}
