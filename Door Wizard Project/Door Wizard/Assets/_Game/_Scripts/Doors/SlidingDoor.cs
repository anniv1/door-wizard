﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlidingDoor : OpenCloseDoor
{
    [Header("Sliding Door")]
    [SerializeField] float slideDistance = 2.3f;
    public Transform spawnPos;
    public Collider portalCollider1 = null; //active on default
    public Collider portalCollider2 = null; //inactive on default
    public SlidingDoor exitPortal1 = null;
    public SlidingDoor exitPortal2 = null;
    public string teleportLayer;
    int teleportLayerInt;
    
    Transform exitSpawnPos;

    private void Start()
    {
        teleportLayerInt = LayerMask.NameToLayer(teleportLayer);

        AssignExitSpawnPos(exitPortal1);
    }

    public void AssignExitSpawnPos(SlidingDoor exitPortal)
    {
        if (exitPortal != null)
        {
            exitSpawnPos = exitPortal.spawnPos;
        }
        else
        {
            exitSpawnPos = spawnPos;
        }
    }

    public override void Open(GameObject door)
    {
        //move door
        base.Open(door);
        Vector3 newPos = Vector3.right * slideDistance * transform.localScale.x;
        door.transform.Translate(newPos, Space.Self);
        spawnPos.transform.Translate(-newPos, Space.Self);

        exitSpawnPos = exitPortal2.spawnPos;
        portalCollider1.enabled = false;
        portalCollider2.enabled = true;

        ChangeExitPortal(exitPortal2, false);
    }

    public override void Close(GameObject door)
    {
        //move door
        base.Close(door);
        Vector3 newPos = -Vector3.right * slideDistance * transform.localScale.x;
        door.transform.Translate(newPos, Space.Self);
        spawnPos.transform.Translate(-newPos, Space.Self);

        ChangeExitPortal(exitPortal1, true);
    }

    void ChangeExitPortal(SlidingDoor exitPortal, bool changeToPortal1)
    {
        if (exitPortal != null)
        {
            exitSpawnPos = exitPortal.spawnPos;
            portalCollider1.enabled = changeToPortal1;
            portalCollider2.enabled = !changeToPortal1;
        }
        else
        {
            return;
        }
    }

    protected void OnCollisionEnter(Collision collision)
    {
        Teleport(collision, exitSpawnPos);
    }

    protected virtual void Teleport(Collision collision, Transform spawnPos)
    {
        if (collision.gameObject.layer == teleportLayerInt) //change layerMask accordingly
        {
            if (spawnPos != null)
            {
                collision.gameObject.transform.position = new Vector3(spawnPos.position.x, spawnPos.position.y, spawnPos.position.z);
            }
        }
    }
}
