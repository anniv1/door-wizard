﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetDoor : ClassicDoor
{
    private void OnCollisionEnter(Collision collision)
    {
        if (isOpen)
        {
            UIManager.Instance.OpenScreen(UIScreens.ResetScreen);
        }
    }

    protected override void OnRegisterObjects()
    {
        
    }
}
