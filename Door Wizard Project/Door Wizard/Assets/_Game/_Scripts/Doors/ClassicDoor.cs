﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClassicDoor : OpenCloseDoor
{
    [Header("Classic Door")]
    public Collider doorCollider = null;
    protected Vector3 rotAngle = new Vector3(0f, -90f, 0f);

    protected virtual void Start()
    {
  
    }

    public override void Open(GameObject door)
    {
        base.Open(door);
        door.transform.localRotation = Quaternion.Euler(rotAngle);
        AudioManager.Instance.PlaySFXClip(SFXClips.OpenDoor);
        doorCollider.enabled = true;
    }

    public override void Close(GameObject door)
    {
        base.Close(door);
        door.transform.localRotation = Quaternion.Euler(Vector3.zero);
        AudioManager.Instance.PlaySFXClip(SFXClips.CloseDoor);
        doorCollider.enabled = false;
    }
}
