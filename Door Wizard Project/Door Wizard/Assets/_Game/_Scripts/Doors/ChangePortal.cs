﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangePortal : MonoBehaviour
{
    [SerializeField] float manaCost = 5;
  //  [SerializeField] SinglePortal portal = null;
    [SerializeField] int newExitRoomID = 0;
    [SerializeField] int newExitPortalID = 0;
    [SerializeField] Material newPortalMat = null;

    private void OnTriggerStay(Collider other)
    {
        if (Input.GetKeyDown(KeyCode.X))
        {
            if (StatsManager.Instance.CheckMana(manaCost))
            {
                //portal.exitRoomID = newExitRoomID;
                //portal.exitPortalID = newExitPortalID;
                //portal.GetComponent<Renderer>().material = newPortalMat;

                StatsManager.Instance.AddMana(-manaCost);
                manaCost = 0;
            }
        }
    }
}
