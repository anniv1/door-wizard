﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TwoWayPortalDoor : PortalDoor
{
    [Header("Two-Way Portal Door")]
    [SerializeField] bool canBeSpawned = false;
    public TwoWayPortalDoor exitPortal;
    public Transform spawnPos;
    [HideInInspector] public bool doTeleport = true;

    [SerializeField] List<Renderer> doorRenderers = new List<Renderer>();
    List<Material> initDoorMaterials = new List<Material>();

    protected string teleportEnableDelay = "TeleportEnableDelay";
    protected string clearPortalDelay = "ClearPortalDelay";

    public override void Open(GameObject door)
    {
        base.Open(door);
    }

    public override void Close(GameObject door)
    {
        base.Close(door);

        if (canBeSpawned)
        {
            StartCoroutine(clearPortalDelay);
        }
    }

    public override void OpenExitDoor()
    {
        if (exitPortal == null)
        {
            GetExitDoor();
        }

        exitPortal.Open(exitPortal.door);
    }

    public void GetExitDoor()
    {
        if (exitPortal == null)
        {
            exitPortal = GetComponent<TwoWayPortalDoor>();
        }
        
        exitPortal.exitPortal = this;
    }

    public override void CloseExitDoor()
    {
        if (exitPortal != null)
        {
            exitPortal.Close(exitPortal.door);
        }
    }

    protected override void Teleport(Collision collision, Transform spawnPos)
    {
        if (doTeleport)
        {
            base.Teleport(collision, exitPortal.spawnPos);
        }
    }

    protected override void DelayNextTeleport()
    {
        StartCoroutine(teleportEnableDelay);
    }

    protected IEnumerator TeleportEnableDelay()
    {
        doTeleport = false;
        exitPortal.doTeleport = false;
        doorCollider.enabled = false;

        yield return new WaitForSeconds(0.5f);

        doTeleport = true;
        exitPortal.doTeleport = true;
        doorCollider.enabled = true;
    }

    protected IEnumerator ClearPortalDelay()
    {
        ParticlesManager.Instance.InstantiateParticle(Particles.CloseDoor, transform.position, transform);
        ParticlesManager.Instance.InstantiateParticle(Particles.CloseDoor, exitPortal.transform.position, exitPortal.transform);
        yield return new WaitForSeconds(0.5f);
        EventManager.TriggerEvent(Events.ClearPortalDoors);
    }

    public override void OnUnlockDoor()
    {
        base.OnUnlockDoor();

        KeyManager.Data data = EventManager.GetTriggerData<KeyManager.Data>();
        if (data != null && data.currentKeyType == keyType)
        {
            ParticlesManager.Instance.InstantiateParticle(Particles.UnlockDoor, transform.position, transform);

            for (int i = 0; i < doorRenderers.Count; i++)
            {
                doorRenderers[i].material = initDoorMaterials[i];
            }
        }
    }

    protected override void OnRegisterObjects()
    {
        if (isLocked)
        {
            if (exitPortal != null)
            {
                DoorManager.Instance.RegisterDoor(this);

                if (!canBeSpawned)
                {
                    for (int i = 0; i < doorRenderers.Count; i++)
                    {
                        initDoorMaterials.Add(doorRenderers[i].material);
                        doorRenderers[i].material = MaterialManager.Instance.disperseMat;
                    }
                }
            }
        }
    }
}
