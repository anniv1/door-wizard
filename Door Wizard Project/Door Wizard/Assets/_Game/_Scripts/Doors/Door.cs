﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    public KeyTypes keyType = 0;
    public DoorTypes doorType = 0;

    protected virtual void OnEnable()
    {
        EventManager.StartListening(Events.RegisterObjects, OnRegisterObjects);
    }

    protected virtual void OnDisable()
    {
        EventManager.StopListening(Events.RegisterObjects, OnRegisterObjects);
    }

    protected virtual void OnRegisterObjects()
    {
        DoorManager.Instance.RegisterDoor(this);
    }
}   
