﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Touch;
using UnityEngine.EventSystems;

public class PreviewDoor : PortalDoor
{
    public PreviewDoorTypes previewDoorType;
    public Camera cam;
    public float range;
    public LayerMask groundMask;
    public Renderer doorRend;
    public Renderer doorFrameRend;
    public Material previewMat; //door standard preview colour
    public Material cannotPlaceMat; //to be applied when the door is over something that can't place doors

    Ray ray;
    RaycastHit hit;

    public Vector3 prevPosToSpawn;
    public Quaternion prevRotToSpawn;
    protected override void OnEnable()
    {
        base.OnEnable();
        LeanTouch.OnFingerUpdate += HandleFingerUpdate;
        LeanTouch.OnFingerUp += HandleFingerUp;

        EventManager.StartListening(Events.LoadLevel, OnLoadLevel);
        prevPosToSpawn = transform.position;
        prevRotToSpawn = transform.rotation;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        LeanTouch.OnFingerUpdate -= HandleFingerUpdate;
        LeanTouch.OnFingerUp -= HandleFingerUp;

        EventManager.StopListening(Events.LoadLevel, OnLoadLevel);
    }

    protected override void Start()
    {
        doorRend.material = previewMat;
        doorFrameRend.material = previewMat;

        cam = CameraControls.Instance.GetCurrentCamera();
    }

    void HandleFingerUpdate(LeanFinger finger)
    {

        ray = finger.GetStartRay(cam);
        if (Physics.Raycast(ray, out hit, range, groundMask))
        {
            if (!EventSystem.current.IsPointerOverGameObject())
            {

                prevPosToSpawn = finger.ScreenPosition; //slightly off for some reason

                transform.Rotate(0, 1, 0);

                prevPosToSpawn = transform.position;
                prevRotToSpawn = transform.rotation;
            }
            
        }
        else
        {
            CannotPlacePreview();
        }
    }

    public void CannotPlacePreview()
    {
        doorRend.material = cannotPlaceMat;
        doorFrameRend.material = cannotPlaceMat;
    }

    //when finger is up, save spawn data to the manager 
    void HandleFingerUp(LeanFinger finger)
    {
        prevPosToSpawn = transform.position;
        prevRotToSpawn = transform.rotation;

    }

    void OnLoadLevel()
    {
        Start();
    }
}
