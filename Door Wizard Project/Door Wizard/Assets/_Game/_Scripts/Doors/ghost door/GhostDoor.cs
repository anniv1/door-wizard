﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostDoor : Door
{
    protected override void OnEnable()
    {
        base.OnEnable();
        if (GhostModeManager.Instance.isGhostModeEnabled == true)
        {
            OnEnableGhostMode();
        }
        else
        {
            OnDisableGhostMode();
        }

        EventManager.StartListening(Events.EnableGhostMode, OnEnableGhostMode);
        EventManager.StartListening(Events.DisableGhostMode, OnDisableGhostMode);
     
    } 
    
    protected override void OnDisable()
    {
        base.OnDisable();

        EventManager.StopListening(Events.EnableGhostMode, OnEnableGhostMode);
        EventManager.StopListening(Events.DisableGhostMode, OnDisableGhostMode);
     
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerManager.Instance.PlayerLayerInt || other.gameObject.layer == LayerManager.Instance.GhostModeLayerInt)
        {
            if (GhostModeManager.Instance.isGhostModeEnabled == false)
            {
                EventManager.TriggerEvent(Events.EnableGhostMode);
            }
            else
            {
                EventManager.TriggerEvent(Events.DisableGhostMode);          
            }
        }
    }

    public void OnEnableGhostMode()
    {
        GhostModeManager.Instance.isGhostModeEnabled = true;
        GhostModeManager.Instance.ghostVolume.SetActive(true);
        Debug.Log("ghost mode enabled");

        AudioManager.Instance.PlaySFXClip(SFXClips.Teleport);
    }
    
    public void OnDisableGhostMode()
    {
        GhostModeManager.Instance.isGhostModeEnabled = false;
        GhostModeManager.Instance.ghostVolume.SetActive(false);
        Debug.Log("ghost mode disabled");

        AudioManager.Instance.PlaySFXClip(SFXClips.Teleport);
    }
 
}
