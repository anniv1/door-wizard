﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.HighDefinition;

public class PortalDoor : ClassicDoor
{
    [Header("Portal Door")]
    [Tooltip("Only used for one-way portals!")]  public Transform exitSpawnPos = null;
    public string teleportLayer = "";
    public bool deactivates;
    int teleportLayerInt;

    protected override void Start()
    {
        base.Start();
        teleportLayerInt = LayerMask.NameToLayer(teleportLayer);
    }

    protected void OnCollisionEnter(Collision collision)
    {
        Teleport(collision, exitSpawnPos);
    }

    protected virtual void Teleport(Collision collision, Transform spawnPos)
    {
        if (collision.gameObject.layer == teleportLayerInt || collision.gameObject.layer == LayerManager.Instance.GhostModeLayerInt) //change layerMask accordingly 
        {
            if (spawnPos != null)
            {
                collision.gameObject.transform.position = new Vector3(spawnPos.position.x, spawnPos.position.y, spawnPos.position.z);
                AudioManager.Instance.PlaySFXClip(SFXClips.Teleport);

                DelayNextTeleport();

                if (deactivates)
                {
                    //destroys portals, so they can only be used once
                    EventManager.TriggerEvent(Events.ClearPortalDoors);
                }
            }
        }
    }

    protected virtual void DelayNextTeleport()
    {

    }
}
