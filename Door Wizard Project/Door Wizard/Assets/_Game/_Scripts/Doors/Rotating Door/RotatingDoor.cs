﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingDoor : MonoBehaviour
{
    public GameObject door;

    public Transform redExit;
    public Transform blueExit;

    public Material redMat;
    public Material blueMat;
    public Renderer doorRend;

    private void OnEnable()
    {
        EventManager.StartListening(Events.ColourSwitch, OnColorSwitch);
    }

    private void OnDisable()
    {
        if (EventManager.Instance != null)
        {
            EventManager.StopListening(Events.ColourSwitch, OnColorSwitch);
        }
    }

    void Start()
    {
        SetStartingColor();
    }
    private void Update()
    {
        if (PuzzleManager.Instance.blueSwitchDown)
        {
            door.transform.Rotate(0, 1, 0);
            doorRend.material = blueMat;
        }
        else
        {
            door.transform.Rotate(0, -1, 0);
            doorRend.material = redMat;
        }
    }

    void SetStartingColor()
    {
        if (PuzzleManager.Instance.blueSwitchDown)
        {
            doorRend.material = blueMat;
        }
        else
        {
            doorRend.material = redMat;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerManager.Instance.PlayerLayerInt)
        {
            //spawn to different positions depending on rotation
            if (PuzzleManager.Instance.blueSwitchDown)
            {
                other.transform.position = blueExit.position; //blue
            }
            else
            {
                other.transform.position = redExit.position; //red
            }
        }
    }

    void OnColorSwitch()
    {        
        if (PuzzleManager.Instance.blueSwitchDown)
        {
            doorRend.material = blueMat;
        }
        else
        {
            doorRend.material = redMat;
        }
    }
}
