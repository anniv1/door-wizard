﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingExit : MonoBehaviour
{
    public Transform entrance;
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.transform.position = entrance.transform.position;
        }
    }
}
