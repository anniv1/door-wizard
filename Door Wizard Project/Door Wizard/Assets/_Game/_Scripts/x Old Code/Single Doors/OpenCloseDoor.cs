﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenCloseDoor : Door
{
    [SerializeField] public GameObject door = null;
    public bool isOpen;
    public bool isLocked = false;
    public bool isFinalDoor;

    // Shaking
    bool doShake = false;
    Vector3 initPos;
    float shakeAmplitude = 20f;
    float shakeDuration = 0.2f;

    bool playerDetected = false;

    protected override void OnEnable()
    {
        base.OnEnable();
        EventManager.StartListening(Events.UnlockDoor, OnUnlockDoor);

        initPos = transform.position;
        
        if (isOpen)
        {
            Open(door);
        }

        if (keyType > 0)
        {
            isLocked = true;
        }
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        EventManager.StopListening(Events.UnlockDoor, OnUnlockDoor);
    }

    public virtual void Update()
    {
        if (playerDetected)
        {
            if (Input.GetKeyDown(KeyCode.Space) || GameManager.Instance.isDoorOpen == true)
            {
                if (isLocked == false)
                {
                    if (!isOpen)
                    {
                        Open(door);
                        OpenExitDoor();
                    }
                    else
                    {
                        Close(door);
                        CloseExitDoor();
                    }
                }
                else
                {
                    Shake();
                    AudioManager.Instance.PlaySFXClip(SFXClips.LockedDoor);
                }
            }
        }
    }

    protected virtual void OnTriggerStay(Collider other)
    {
        if (other.gameObject.layer == LayerManager.Instance.PlayerLayerInt || other.gameObject.layer == LayerManager.Instance.GhostModeLayerInt)
        {
            playerDetected = true;
            GameManager.Instance.isDoorOpen = false;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == LayerManager.Instance.PlayerLayerInt || other.gameObject.layer == LayerManager.Instance.GhostModeLayerInt)
        {
            playerDetected = false;
        }
    }

    public void OpenDoor()
    {
        
    }

    public virtual void Open(GameObject door)
    {
        isOpen = true;
    }

    public virtual void Close(GameObject door)
    {
        isOpen = false;
        GameManager.Instance.isDoorOpen = false;
    }

    public virtual void OpenExitDoor()
    {

    }

    public virtual void CloseExitDoor()
    {

    }

    #region Shake()
    protected void FixedUpdate()
    {
        if (doShake == true)
        {
            Vector3 newPos = initPos + Random.insideUnitSphere * (Time.deltaTime * shakeAmplitude);
            newPos.y = transform.position.y;
            newPos.z = transform.position.z;

            transform.position = newPos;
        }
    }

    protected void Shake()
    {
        StartCoroutine("Shaking");
    }

    protected IEnumerator Shaking()
    {
        if (doShake == false)
        {
            doShake = true;
        }

        yield return new WaitForSeconds(shakeDuration);

        doShake = false;
        transform.position = initPos;

        StopAllCoroutines();
    }
    #endregion

    public virtual void OnUnlockDoor()
    {
        if (KeyManager.Instance.CheckKey(keyType))
        {
            isLocked = false;
        }
    }

    //void OnLoadLevel()
    //{
    //    OnRegisterObjects();
    //}
}
