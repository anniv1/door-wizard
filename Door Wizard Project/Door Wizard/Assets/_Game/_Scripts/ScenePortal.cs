﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScenePortal : MonoBehaviour
{
    [HideInInspector] public string nextLevel;
    

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerManager.Instance.PlayerLayerInt)
        {
            if (nextLevel != null)
            {
                GameManager.Instance.isPaused = true;
                EventManager.TriggerEvent(Events.PreLoadLevel);
                UIManager.Instance.LoadScene(nextLevel);
            }
        }
    }

  
}
