﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ControlFreak2;
using UnityEngine;
using Yarn.Unity;

public class PlayerMovement : MonoBehaviour
{
    public float distanceToGround = 0.5f;
    public int moveSpeed;
    public float fallMultiplier = 2.5f;

    public Material ghostModeMat;
    List<Renderer> handelleRenderers = new List<Renderer>();
    List<Material> initHandelleMats = new List<Material>();
    public GameObject lilypad = null;

    Rigidbody rbody;
    string knockbackDelay = "KnockbackDelay";

    public Animator anim;
    public float interactionRadius = 2.0f;

    public List<ContactPoint> contactPos = new List<ContactPoint>();
    Vector3 pushNormal;
    Vector3 pushDirection;

    float vertical;
    float horizontal; 
    
    float tVertical;
    float tHorizontal;

    bool pushingBlock = false;
    float idleTime;
    float idleMax = 50;
    bool reassignInitPos = false;
    bool isGhost = false;
    

    [HideInInspector] public Vector3 initPos = Vector3.zero;

    private void OnEnable()
    {
        EnableAllDirections();

        EventManager.StartListening(Events.RegisterObjects, OnRegisterObjects);
        EventManager.StartListening(Events.PlayerKnockback, OnPlayerKnockback);
        EventManager.StartListening(Events.EnterRoom, OnEnterRoom);
        EventManager.StartListening(Events.EnableGhostMode, OnEnableGhostMode);
        EventManager.StartListening(Events.DisableGhostMode, OnDisableGhostMode);
        EventManager.StartListening(Events.RunDialogue, OnRunDialogue);

        initPos = transform.position;
        rbody = GetComponent<Rigidbody>();
        handelleRenderers = GetComponentsInChildren<Renderer>().ToList();
        for (int i = 0; i < handelleRenderers.Count; i++)
        {
            initHandelleMats.Add(handelleRenderers[i].material);
        }

        isGhost = false;
    }

    private void OnDisable()
    {
        EventManager.StopListening(Events.RegisterObjects, OnRegisterObjects);
        EventManager.StopListening(Events.PlayerKnockback, OnPlayerKnockback);
        EventManager.StopListening(Events.EnterRoom, OnEnterRoom);
        EventManager.StopListening(Events.EnableGhostMode, OnEnableGhostMode);
        EventManager.StopListening(Events.DisableGhostMode, OnDisableGhostMode);
        EventManager.StopListening(Events.RunDialogue, OnRunDialogue);        
    }

    private void Update()
    {
        if (transform.position.y < -10f)
        {
            transform.position = initPos;
        }

        if (GameManager.Instance.playerCanMove)
        {
            Movement();
            TouchMovement();
        }

        //to make the fall more weighted
        if (rbody.velocity.y < 0)
        {
            rbody.velocity += Vector3.up * Physics.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
        }

        //CHECK INPUT
        if (Input.anyKeyDown)
        {
            //reset velocity after collision
            rbody.velocity = Vector3.zero;
            anim.SetBool("isInteracting", false);
            anim.SetBool("isIdle", false);
            anim.SetBool("isCasting", false);
            anim.SetBool("isVictory", false);
            anim.SetBool("isWaving", false);
        }

    }



    bool checkIfPressingSameDirAsPushing(Directions pushDir, float horizontalAxis, float verticalAxis)
    {
        float deadZoneAmt = 0.5f;

        switch (pushDir)
        {
            case Directions.UpRight:
            {
                    if (verticalAxis > deadZoneAmt && horizontalAxis > deadZoneAmt)
                    {
                        Debug.Log("up right");
                        return true;
                    }
                break;
            }
            
            case Directions.UpLeft:
            {
                    if (verticalAxis < deadZoneAmt && horizontalAxis > deadZoneAmt)
                    {
                        Debug.Log("up left");
                        return true;
                    }
                break;
            }
            case Directions.DownLeft:
            {
                    if (verticalAxis < deadZoneAmt && horizontalAxis < deadZoneAmt)
                    {
                        Debug.Log("down left");
                        return true;
                    }
                break;
            }
            case Directions.DownRight:
            {
                    if (verticalAxis > deadZoneAmt && horizontalAxis < deadZoneAmt)
                    {
                        Debug.Log("down right");
                        return true;
                    }
                break;
            }

            default: break;
        }

        return false;
    }
     
    Directions getDirectionsFromFacing(Vector3 facing)
    {
        for (int i = 0; i < (int)Directions.COUNT; i++)
        {
            switch ((Directions)i)
                {
                    case Directions.UpRight:
                        {
                            if (facing == new Vector3(1,0,1))
                            {
                           
                            return (Directions)i;
                        
                            }
                            break;
                        }
                
                case Directions.UpLeft:
                        {
                            if (facing == new Vector3(-1,0,1)) 
                            {
                            
                            return (Directions)i;
                            }
                            break;
                        } 
                
                case Directions.DownLeft:
                        {
                            if (facing == new Vector3(-1,0,-1)) 
                            {
                         
                            return (Directions)i;
                            }
                            break;
                        }
                    
                case Directions.DownRight:
                        {
                            if (facing == new Vector3(1,0,-1)) 
                            {
                      
                            return (Directions)i;
                            }
                            break;
                        }

                    default: break;
                }
        }
      

        return Directions.COUNT;
    }

    void EnableAllDirections()
    {
        vertical = Input.GetAxis("Vertical");
        horizontal = Input.GetAxis("Horizontal");

        tVertical = CF2Input.GetAxis("Vertical");
        tHorizontal = CF2Input.GetAxis("Horizontal");
    }
   
    void Movement()
    {
        //MOVEMENT WITH WASD
        float moveVertical = Input.GetAxis("Vertical");//vertical; 
        float moveHorizontal = Input.GetAxis("Horizontal");

        anim.SetFloat("horizontal", Input.GetAxisRaw("Horizontal"));
        anim.SetFloat("vertical", Input.GetAxisRaw("Vertical"));

        //put this in an if statement if not touching block
        Vector3 newPosition = Vector3.zero;

        PushingBlockCheck(newPosition, moveVertical, moveHorizontal);

        transform.LookAt(newPosition + transform.position); //so the player looks in the direction it is moving in
        transform.Translate(newPosition * moveSpeed * Time.deltaTime, Space.World);

       

    }

    void TouchMovement()
    {
        //TOUCH JOYSTICK MOVEMENT
        float touchMoveVertical = CF2Input.GetAxisRaw("Vertical");
        float touchMoveHorizontal = CF2Input.GetAxisRaw("Horizontal");

        anim.SetFloat("horizontal", CF2Input.GetAxisRaw("Horizontal"));
        anim.SetFloat("vertical", CF2Input.GetAxisRaw("Vertical"));


        Vector3 newTouchPosition = new Vector3(touchMoveHorizontal, 0.0f, touchMoveVertical); //when the joystick is moved the player moves in that direction

        PushingBlockCheck(newTouchPosition, touchMoveHorizontal, touchMoveVertical);


        transform.LookAt(newTouchPosition + transform.position); //so the player looks in the direction it is moving in
        transform.Translate(newTouchPosition * moveSpeed * Time.deltaTime, Space.World);

        if (Input.GetAxisRaw("Vertical") != 0 || Input.GetAxisRaw("Horizontal") != 0)
        {
            if (!CameraControls.Instance.IsCamFollowEnabled())
            {
                CameraControls.Instance.ResetCamera();
            }

            EventManager.TriggerEvent(Events.PlayerMove);
        }


    }

    void PushingBlockCheck(Vector3 newPos, float moveHor, float moveVert)
    {
        if (pushingBlock == true)
        {

            //check if pressing opposite
            if (checkIfPressingSameDirAsPushing(getDirectionsFromFacing(pushDirection), moveHor, moveVert) == false)
            {
                pushingBlock = false; //if you can't get this to work before submission, swap the true and false for pushing block in this if statement
                if (vertical == 0 && horizontal == 0)
                {
                    idleTime++;
                    Debug.Log("idle time" + idleTime);
                }

            }

            else //check if pressing the correct way
            {
                //if true set newposition  
                newPos = pushDirection.normalized;
            }

        }
        else //if pushing block is false
        {
            newPos = new Vector3(moveHor, 0.0f, moveVert); //when aswd or the directional keys are pressed the player moves in that direction

            if (idleTime > idleMax)
            {

                EnableAllDirections();
                idleTime = 0;
            }

        }
   
    }



    private void OnCollisionEnter(Collision collision)
    {
        if (gameObject.layer != LayerManager.Instance.GhostModeLayerInt)
        {
            if (collision.gameObject.layer == LayerManager.Instance.PushBlockLayerInt)
            {
                pushingBlock = true;

                //get the nomrmal at the collision point
                collision.GetContacts(contactPos); //puts the contact positions in the contact positions list
                pushNormal = contactPos[0].normal.normalized;

                //flip the normal to get the pushing direction
                pushDirection = pushNormal * -1; //inverse to get the direction of the push


            }
            else if (collision.gameObject.layer == LayerManager.Instance.WaterLayerInt)
            {
                lilypad.SetActive(true);
                anim.SetBool("isRowing", true);
            }
        }

        if (collision.gameObject.layer == LayerManager.Instance.GroundLayerInt || collision.gameObject.layer == LayerManager.Instance.TerrainLayerInt)
        {
            if (reassignInitPos)
            {
                initPos = transform.position;
                reassignInitPos = false;
            }
        }
    }


    private void OnCollisionExit(Collision collision)
    {
        if (gameObject.layer != LayerManager.Instance.GhostModeLayerInt)
        {
            if (collision.gameObject.layer == LayerManager.Instance.PushBlockLayerInt)
            {
                pushingBlock = false;

                //if the player moves in the exact opposite direction, re-enable all directional movement
                EnableAllDirections();
            }
            else if (collision.gameObject.layer == LayerManager.Instance.WaterLayerInt)
            {
                lilypad.SetActive(false);
                anim.SetBool("isRowing", false);
            }
        }
    }

    void OnRegisterObjects()
    {
        CameraControls.Instance.RegisterPlayer(this);
    }

    void OnPlayerKnockback()
    {
        BubbleBarrier.Data data = EventManager.GetTriggerData<BubbleBarrier.Data>();
        if (data != null)
        {
            GameManager.Instance.playerCanMove = false;
            rbody.AddForce(data.currentKnockBackDir * data.currentKnockbackForce, ForceMode.Impulse);
            StartCoroutine(knockbackDelay);
        }
    }

    void OnEnterRoom()
    {
        reassignInitPos = true;
    }

    /// Find all DialogueParticipants
    /** Filter them to those that have a Yarn start node and are in range; 
     * then start a conversation with the first one
     */
    public void CheckForNearbyNPC()
    {
        var allParticipants = new List<NPCs>(FindObjectsOfType<NPCs>());
        var target = allParticipants.Find(delegate (NPCs p) {
            return string.IsNullOrEmpty(p.talkToNode) == false && // has a conversation node?
            (p.transform.position - this.transform.position)// is in range?
            .magnitude <= interactionRadius;
        });
        if (target != null)
        {
            // Kick off the dialogue at this node.
            FindObjectOfType<DialogueRunner>().StartDialogue(target.talkToNode);
        }
    }

    /// Draw the range at which we'll start talking to people.
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;

        // Need to draw at position zero because we set position in the line above
        Gizmos.DrawWireSphere(Vector3.zero, interactionRadius);
    }

    void OnEnableGhostMode()
    {
        isGhost = true;
        SetPlayerLayerTo(LayerManager.Instance.GhostModeLayerInt);
        SetPlayerMaterialsTo(ghostModeMat);
    }

    void OnDisableGhostMode()
    {
        isGhost = false;
        SetPlayerLayerTo(LayerManager.Instance.PlayerLayerInt);
        SetPlayerMaterialsTo(initHandelleMats);
    }

    void SetPlayerLayerTo(int newLayer)
    {
        gameObject.layer = newLayer;
    }

    void SetPlayerMaterialsTo(Material newMaterial)
    {
        for (int i = 0; i < handelleRenderers.Count; i++)
        {
            handelleRenderers[i].material = newMaterial;
        }
    }

    void SetPlayerMaterialsTo(List<Material> newMaterialList)
    {
        for (int i = 0; i < handelleRenderers.Count; i++)
        {
            handelleRenderers[i].material = newMaterialList[i];
        }
    }

    IEnumerator KnockbackDelay()
    {
        yield return new WaitForSeconds(0.3f);
        GameManager.Instance.playerCanMove = true;
    }

    void OnRunDialogue()
    {
        GameManager.Instance.playerCanMove = false;
    }
}

