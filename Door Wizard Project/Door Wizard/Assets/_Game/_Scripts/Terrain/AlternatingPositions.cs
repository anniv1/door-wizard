﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlternatingPositions : AlternatingTerrain
{
    [SerializeField] List<Transform> positions = new List<Transform>();

    protected override void GenerateTerrain()
    {
        base.GenerateTerrain();
        GeneratePosition();
    }

    protected override void GenerateDependentTerrain(int prerequisiteInt)
    {
        base.GenerateDependentTerrain(prerequisiteInt);
        positions.RemoveAt(prerequisiteInt);
        GeneratePosition();
    }

    void GeneratePosition()
    {
        randomInt = Random.Range(0, positions.Count);
        transform.position = positions[randomInt].position;
    }
}
