﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AlternatingActiveChildren : AlternatingTerrain
{
    [Header("Alternating Active Children")]
    [SerializeField] bool setOnlyOneActive = false;

    List<AlternatingActiveSelf> children = new List<AlternatingActiveSelf>();
    bool atLeastOneIsActive = false;

    protected override void GenerateTerrain()
    {
        base.GenerateTerrain();
        children = GetComponentsInChildren<AlternatingActiveSelf>(true).ToList();

        if (setOnlyOneActive)
        {
            int randomChild = Random.Range(0, children.Count);
            for (int i = 0; i < children.Count; i++)
            {
                if (i == randomChild)
                {
                    children[i].gameObject.SetActive(true);
                }
                else
                {
                    children[i].gameObject.SetActive(false);
                }
            }
        }
        else
        {
            for (int i = 0; i < children.Count; i++)
            {
                if (Random.value > 0.5f)
                {
                    children[i].gameObject.SetActive(true);

                    if (!atLeastOneIsActive)
                    {
                        atLeastOneIsActive = true;
                    }
                }
                else
                {
                    children[i].gameObject.SetActive(false);
                }
            }

            if (!atLeastOneIsActive)
            {
                children[0].gameObject.SetActive(true);
            }
        }
    }
}
