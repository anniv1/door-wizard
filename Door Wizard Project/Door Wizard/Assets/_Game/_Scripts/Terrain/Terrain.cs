﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class Terrain : MonoBehaviour
{
    GameObject terrain = null;
    List<Renderer> terrainRends = new List<Renderer>();

    bool activateTerrainColor = false;
    bool resetTerrainColor = false;
    Color initTerrainColor;
    float t = 0f;
    string baseColor = "_BaseColor";
    float colorChangeSpeed = 1.5f;

    void Start()
    {
        terrainRends = terrain.GetComponentsInChildren<Renderer>().ToList();
        initTerrainColor = terrainRends[0].material.GetColor(baseColor);
    }

    void Update()
    {
        if (activateTerrainColor)
        {
            ChangeGroundColor(terrainRends, MaterialManager.Instance.activeGroundCol);
        }
        else if (resetTerrainColor)
        {
            ChangeGroundColor(terrainRends, initTerrainColor);
        }
    }

    void ChangeGroundColor(List<Renderer> rends, Color endCol)
    {
        if (t < 1f)
        {
            t += Time.deltaTime * colorChangeSpeed;
            for (int i = 0; i < terrainRends.Count; i++)
            {
                terrainRends[i].material.SetColor(baseColor, Color.Lerp(rends[i].material.GetColor(baseColor), endCol, t));
            }
        }
        else
        {
            if (activateTerrainColor)
            {
                activateTerrainColor = false;
            }
            else if (resetTerrainColor)
            {
                resetTerrainColor = false;
            }
        }
    }

}
