﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlternatingTerrain : MonoBehaviour
{
    [SerializeField] protected bool isPrerequisite = false;
    [SerializeField] protected bool isDependent = false;
    protected int randomInt = 0;
        
    [SerializeField] protected AlternatingTerrain dependentTerrain = null;

    private void OnEnable()
    {
        GenerateAlternatingTerrain();
    }

    void GenerateAlternatingTerrain()
    {
        //if dependent, wait until after prerequisite terrain has finished
        if (isDependent)
        {
            return;
        }

        GenerateTerrain();

        if (isPrerequisite)
        {
            dependentTerrain.GenerateDependentTerrain(randomInt);
        }
    }

    protected virtual void GenerateTerrain()
    {
        //generate terrain as normal
    }

    protected virtual void GenerateDependentTerrain(int prerequisiteInt)
    {
        
    }
}
