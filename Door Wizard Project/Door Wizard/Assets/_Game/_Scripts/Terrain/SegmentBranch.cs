﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SegmentBranch : MonoBehaviour
{
    public List<TwoWayPortalDoor> segmentPortals = new List<TwoWayPortalDoor>();

    private void OnEnable()
    {
        OnRegisterObjects();
    }

    void OnRegisterObjects()
    {
        segmentPortals = GetComponentsInChildren<TwoWayPortalDoor>().ToList();

        if(segmentPortals.Count > 0)
        {
            SegmentManager.Instance.RegisterSegmentBranch(this);
        }
    }
}
