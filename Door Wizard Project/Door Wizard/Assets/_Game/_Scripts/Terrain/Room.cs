﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering;

public class Room : MonoBehaviour
{

    [Header("Ground")]
    [SerializeField] GameObject ground = null;
    List<Transform> grounds = new List<Transform>();
    List<Renderer> groundRends = new List<Renderer>();

    // Color Change
    Color initGroundCol = Color.black;
    bool activateGroundColor = false;
    bool resetGroundColor = false;
    float t = 0f;
    string baseColor = "_BaseColor";
    float colorChangeSpeed = 1.5f;

    private void Awake()
    {
        //get ground objects
        grounds = ground.GetComponentsInChildren<Transform>().ToList();

        //get ground renderer and color
        groundRends = ground.GetComponentsInChildren<Renderer>().ToList();
        if (groundRends.Count != 0)
        {
            initGroundCol = groundRends[0].material.GetColor(baseColor);
        }
    }

    private void OnEnable()
    {
        EventManager.StartListening(Events.RegisterObjects, OnRegisterObjects);
        EventManager.StartListening(Events.UpdateActiveRooms, OnUpdateActiveRooms);
    }

    private void OnDisable()
    {
        EventManager.StopListening(Events.RegisterObjects, OnRegisterObjects);
        EventManager.StopListening(Events.UpdateActiveRooms, OnUpdateActiveRooms);
    }

    void Update()
    {
        if (activateGroundColor)
        {
            ChangeGroundColor(groundRends, MaterialManager.Instance.activeGroundCol);
        }
        else if (resetGroundColor)
        {
            ChangeGroundColor(groundRends, initGroundCol);
        }
    }   

    #region ChangeGroundColor() + overrides
    void ChangeGroundColor(Color startCol, Color endCol)
    {
        //lerp color change
        if (t < 1f)
        {
            t += Time.deltaTime * colorChangeSpeed;
            for (int i = 0; i < groundRends.Count; i++)
            {
                //change material "Base Color"
                groundRends[i].material.SetColor(baseColor, Color.Lerp(startCol, endCol, t));
            }
        }
        else
        {
            if (activateGroundColor)
            {
                activateGroundColor = false;
            }
            else if (resetGroundColor)
            {
                resetGroundColor = false;
            }
        }
    }

    void ChangeGroundColor(List<Renderer> rends, Color endCol) 
    {
        if (t < 1f)
        {
            t += Time.deltaTime * colorChangeSpeed;
            for (int i = 0; i < groundRends.Count; i++)
            {
                groundRends[i].material.SetColor(baseColor, Color.Lerp(rends[i].material.GetColor(baseColor), endCol, t));
            }
        }
        else
        {
            if (activateGroundColor)
            {
                activateGroundColor = false;
            }
            else if (resetGroundColor)
            {
                resetGroundColor = false;
            }
        }
    }

    void ChangeGroundColor(Color startCol, List<Renderer> rends)
    {
        if (t < 1f)
        {
            t += Time.deltaTime * colorChangeSpeed;
            for (int i = 0; i < groundRends.Count; i++)
            {
                groundRends[i].material.SetColor(baseColor, Color.Lerp(startCol, rends[i].material.GetColor(baseColor), t));
            }
        }
        else
        {
            if (activateGroundColor)
            {
                activateGroundColor = false;
            }
            else if (resetGroundColor)
            {
                resetGroundColor = false;
            }
        }
    }
    #endregion

    private void OnTriggerEnter(Collider other)
    {    
        if (other.gameObject.layer == LayerManager.Instance.PlayerLayerInt || other.gameObject.layer == LayerManager.Instance.GhostModeLayerInt)
        {
            RoomManager.Instance.isInRoom = true;
            EventManager.TriggerEvent(Events.EnterRoom);
            //store the room the player is currently in
            RoomManager.Instance.UpdateCurrentRoom(this, true);
        }   
    }

    private void OnTriggerExit(Collider other)
    {
        
        if (other.gameObject.layer == LayerManager.Instance.PlayerLayerInt)
        {
            RoomManager.Instance.isInRoom = false;
            RoomManager.Instance.UpdateCurrentRoom(this, false);

            if (SpawnDoorManager.Instance.currentDoorType == DoorTypes.ObjectPortal)
            {
                EventManager.TriggerEvent(Events.ClearPortalDoors);
            }
        }
        
    }

    void ActivateGround()
    {
        //change ground to "Ground" layer
        for (int i = 0; i < grounds.Count; i++)
        {
            grounds[i].gameObject.layer = LayerManager.Instance.GroundLayerInt;
        }

        //change color to active
        t = 0;
        resetGroundColor = false;
        activateGroundColor = true;
    }

    void DeactivateGround()
    {
        //change ground to "Default" layer
        for (int i = 0; i < grounds.Count; i++)
        {
            grounds[i].gameObject.layer = 0;
        }

        //change color to normal
        t = 0;
        activateGroundColor = false;
        resetGroundColor = true;
    }

    void OnRegisterObjects()
    {
        RoomManager.Instance.RegisterRoom(this);
    }

    void OnUpdateActiveRooms()
    {
        if (RoomManager.Instance.IsRoomActive(this))
        {
            ActivateGround();
        }
        else
        {
            DeactivateGround();
        }
    }
}
