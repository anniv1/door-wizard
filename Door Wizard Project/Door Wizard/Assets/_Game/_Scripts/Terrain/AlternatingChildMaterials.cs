﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AlternatingChildMaterials : AlternatingTerrain
{
    [SerializeField] List<Material> materials = new List<Material>();
    List<Renderer> childRends = new List<Renderer>();

    protected override void GenerateTerrain()
    {
        base.GenerateTerrain();

        childRends = GetComponentsInChildren<Renderer>().ToList();

        for (int i = 0; i < childRends.Count; i++)
        {
            randomInt = Random.Range(0, materials.Count);
            childRends[i].material = materials[randomInt];

            materials.RemoveAt(randomInt);
        }
        
    }
}
