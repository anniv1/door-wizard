﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialRoom : MonoBehaviour
{
    NPCs currentNPC;
    string setLohkVisible = "SetLohkVisible";

    void Start()
    {
        currentNPC = GetComponentInChildren<NPCs>();
        currentNPC.gameObject.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerManager.Instance.PlayerLayerInt)
        {
            StopAllCoroutines();
            StartCoroutine(setLohkVisible);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == LayerManager.Instance.PlayerLayerInt)
        {
            currentNPC.gameObject.SetActive(false);
            ParticlesManager.Instance.InstantiateParticle(Particles.Lohk, currentNPC.transform.position, transform);
        }
    }

    IEnumerator SetLohkVisible()
    {
        yield return new WaitForSeconds(0.5f);
        currentNPC.gameObject.SetActive(true);
    }
}
