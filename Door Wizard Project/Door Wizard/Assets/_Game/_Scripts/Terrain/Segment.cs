﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Segment : MonoBehaviour
{
    public Segments segmentID = 0;
    public List<SegmentBranch> segmentBranches = new List<SegmentBranch>();

    private void OnEnable()
    {
        EventManager.StartListening(Events.RegisterObjects, OnRegisterObjects);
    }

    private void OnDisable()
    {
        EventManager.StopListening(Events.RegisterObjects, OnRegisterObjects);
    }

    void OnRegisterObjects()
    {
        SegmentManager.Instance.RegisterSegment(this);
        RegisterBranches();
    }

    void RegisterBranches()
    {
        segmentBranches = GetComponentsInChildren<SegmentBranch>().ToList();

        for (int i = 0; i < segmentBranches.Count; i++)
        {
            if (segmentBranches[i].segmentPortals.Count <= 0)
            {
                segmentBranches.RemoveAt(i);
            }
        }
    }
}
