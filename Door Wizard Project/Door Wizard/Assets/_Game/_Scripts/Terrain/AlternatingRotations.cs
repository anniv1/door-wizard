﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlternatingRotations : AlternatingTerrain
{
    [SerializeField] List<Vector3> rotations = new List<Vector3>();

    protected override void GenerateTerrain()
    {
        base.GenerateTerrain();

        int randomRotation = Random.Range(0, rotations.Count);
        transform.localRotation = Quaternion.Euler(rotations[randomRotation]);
    }
}
