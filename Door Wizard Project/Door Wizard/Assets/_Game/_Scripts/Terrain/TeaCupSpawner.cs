﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class TeaCupSpawner : MonoBehaviour
{
    [Header("Tea Cup Spawner")]
    [SerializeField] bool randomSpawnAmount = false;
    [SerializeField] float spawnOffset = 0f;
    [SerializeField] Transform spawnPos = null;
    [SerializeField] List<GameObject> fixedTeaCupPrefabs = new List<GameObject>();
    [SerializeField] List<GameObject> extraTeaCupPrefabs = new List<GameObject>();

    int spawnAmount;

    private void OnEnable()
    {
        EventManager.StartListening(Events.GenerateProceduralTerrain, OnGenerateProceduralTerrain);
    }

    private void OnDisable()
    {
        EventManager.StopListening(Events.GenerateProceduralTerrain, OnGenerateProceduralTerrain);
    }

    void OnGenerateProceduralTerrain()
    {

        //set up variables
        List<GameObject> fixedPrefabOptions = fixedTeaCupPrefabs.ToArray().ToList();
        List<GameObject> extraPrefabOptions = extraTeaCupPrefabs.ToArray().ToList();
        List<GameObject> extraPrefabDuplicateOptions = new List<GameObject>();
        List<TwoWayPortalDoor> connectingPortalOptions = SegmentManager.Instance.allSegmentPortals.ToArray().ToList();

        int portalCount = SegmentManager.Instance.allSegmentPortals.Count;
        if (randomSpawnAmount)
        {

            spawnAmount = Random.Range(portalCount - 2, portalCount);
        }
        else
        {
            spawnAmount = portalCount;
        }

        //store duplicate puzzles
        for (int i = 0; i < extraPrefabOptions.Count; i++)
        {
            DuplicatePuzzle newDuplciatePuzzle = extraPrefabOptions[i].GetComponent<DuplicatePuzzle>();
            if (newDuplciatePuzzle != null)
            {
                extraPrefabDuplicateOptions.Add(extraPrefabOptions[i]);
            }
        }

        float rotateAngle = 360 / spawnAmount;
        spawnPos.localPosition = new Vector3(spawnPos.position.x, spawnPos.position.y, spawnPos.position.z + spawnOffset);

        //spawn tea cups
        for (int i = 0; i < spawnAmount; i++)
        {

            //select and instantiate prefab
            int randomTeaCup;
            GameObject newTeaCup;
            if (fixedPrefabOptions.Count > 0)
            {
                randomTeaCup = Random.Range(0, fixedPrefabOptions.Count);
                newTeaCup = Instantiate(fixedPrefabOptions[randomTeaCup], spawnPos.position, fixedPrefabOptions[randomTeaCup].transform.rotation);
                fixedPrefabOptions.RemoveAt(randomTeaCup);
            }
            else
            {
                randomTeaCup = Random.Range(0, extraPrefabOptions.Count);
                newTeaCup = Instantiate(extraPrefabOptions[randomTeaCup], spawnPos.position, extraPrefabOptions[randomTeaCup].transform.rotation);

                DuplicatePuzzle tempDuplicatePuzzle = newTeaCup.GetComponent<DuplicatePuzzle>();
                if (tempDuplicatePuzzle != null)
                {
                    for (int j = 0; j < extraPrefabDuplicateOptions.Count; j++)
                    {
                        if (tempDuplicatePuzzle.duplicateID == extraPrefabDuplicateOptions[j].GetComponent<DuplicatePuzzle>().duplicateID)
                        {
                            int currentPrefab = extraPrefabOptions.FindIndex(x => x == extraPrefabDuplicateOptions[j]);
                            extraPrefabOptions.RemoveAt(currentPrefab);                            
                        }
                    }
                }
                else
                {
                    extraPrefabOptions.RemoveAt(randomTeaCup);
                }
            }

            //find portals
            TwoWayPortalDoor connectingPortal;
            ConnectedToSegmentRoom newTeaCupRoom = newTeaCup.GetComponent<ConnectedToSegmentRoom>();
            if (newTeaCupRoom.fixedConnection)
            {
                connectingPortal = SegmentManager.Instance.GetSegmentPortal(newTeaCupRoom.connectingSegment);
            }
            else
            {
                int randomPortal = Random.Range(0, connectingPortalOptions.Count);
                connectingPortal = connectingPortalOptions[randomPortal];
                SegmentManager.Instance.SetSegmentBranchInUse(connectingPortal);
            }
            //update list
            connectingPortalOptions.Remove(connectingPortal);

            //connect portal doors
            TwoWayPortalDoor teaCupPortal = newTeaCupRoom.roomPortal;
            TwoWayPortalDoor segmentPortal = connectingPortal;
            teaCupPortal.exitPortal = segmentPortal;
            segmentPortal.exitPortal = teaCupPortal;

            //rotate spawner
            transform.rotation = Quaternion.Euler(transform.eulerAngles.x, transform.eulerAngles.y + rotateAngle, transform.eulerAngles.z);
        }

        //deactivate branches not in use
        SegmentManager.Instance.DeactivateBranchesNotInUse();

        //feedback for unlocked doors
        EventManager.TriggerEvent(Events.UnlockDoor);
    }

}
