﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConnectedToSegmentRoom : MonoBehaviour
{
    public TwoWayPortalDoor roomPortal = null;
    public bool fixedConnection = false;
    public Segments connectingSegment = 0;

}
