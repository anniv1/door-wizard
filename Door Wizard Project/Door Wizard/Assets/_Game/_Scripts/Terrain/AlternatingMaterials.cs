﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlternatingMaterials : AlternatingTerrain
{
    [SerializeField] List<Material> materials = new List<Material>();
    Renderer rend;

    protected override void GenerateTerrain()
    {
        rend = GetComponent<Renderer>();

        randomInt = Random.Range(0, materials.Count);
        rend.material = materials[randomInt];
    }
}
