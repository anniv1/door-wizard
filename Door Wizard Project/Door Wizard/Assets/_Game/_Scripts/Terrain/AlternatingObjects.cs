﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlternatingObjects : AlternatingTerrain
{
    [Header("Alternating Objects")]
    [SerializeField] bool useAllTransforms = true;
    [SerializeField] List<GameObject> prefabs = new List<GameObject>();
    [SerializeField] List<Transform> transforms = new List<Transform>();

    protected override void GenerateTerrain()
    {
        base.GenerateTerrain();

        if (useAllTransforms)
        {
            for (int i = 0; i < transforms.Count; i++)
            {
                int randomPrefab = Random.Range(0, prefabs.Count);
                Instantiate(prefabs[randomPrefab], transforms[i].position, transform.rotation, transforms[i]);
            }
        }
        else
        {
            for (int i = 0; i < prefabs.Count; i++)
            {
                int randomTransforms = Random.Range(0, transforms.Count);
                Instantiate(prefabs[i], transforms[randomTransforms].position, transform.rotation, transform);
            }
        }
    }
}
