﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SegmentPortal : MonoBehaviour
{
    private void OnEnable()
    {
        EventManager.StartListening(Events.RegisterObjects, OnRegisterObjects);
    }

    private void OnDisable()
    {
        EventManager.StopListening(Events.RegisterObjects, OnRegisterObjects);
    }

    void OnRegisterObjects()
    {
        TwoWayPortalDoor currentSegmentPortal = GetComponent<TwoWayPortalDoor>();
        SegmentManager.Instance.RegisterSegmentPortal(currentSegmentPortal);
    }
}
