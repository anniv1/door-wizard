﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AlternatingColors : AlternatingTerrain
{
    [SerializeField] string colorName = "_BaseColor";
    [SerializeField] List<Color> colors = new List<Color>();

    List<Renderer> rends = new List<Renderer>();

    private void Awake()
    {
        rends = GetComponentsInChildren<Renderer>().ToList();
        Renderer rend = GetComponent<Renderer>();
        if (rend != null)
        {
            rends.Add(rend);
        }
    }

    protected override void GenerateTerrain()
    {
        base.GenerateTerrain();

        int randomColor = Random.Range(0, colors.Count);
        for (int i = 0; i < rends.Count; i++)
        {
            rends[i].material.SetColor(colorName, colors[randomColor]);
        }
    }
}
