﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAnimations : MonoBehaviour
{
    
    public Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
        if (Input.GetKeyDown(KeyCode.Space))
        {         
            anim.Play("interaction", 0);
        }

        if (Input.GetMouseButtonDown(0))
        {
            anim.Play("spell cast", 0);
        }


    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerManager.Instance.NPCLayerInt)
        {
            anim.Play("wave", 0);
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == LayerManager.Instance.StarLayerInt)
        {
            anim.Play("victory", 0);
        }
    }
}

