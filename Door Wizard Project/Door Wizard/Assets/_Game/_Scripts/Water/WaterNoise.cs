﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//this code is from this tutorial: 
//[Unity Tutorial] How to Make Low Poly Water: https://www.youtube.com/watch?v=3MoHJtBnn2U

public class WaterNoise : MonoBehaviour
{

    public float power = 3;
    public float scale = 1;
    public float timeScale = 1;

    private float xOffset;
    private float yOffset;
    public MeshFilter mf;

    void Start()
    {
        MakeNoise();
    }

    void Update()
    {
        MakeNoise();
        xOffset += Time.deltaTime * timeScale;
        yOffset += Time.deltaTime * timeScale;
    }

    void MakeNoise()
    {
        Vector3[] vertices = mf.mesh.vertices;

        for (int i = 0; i < vertices.Length; i++)
        {
            vertices[i].y = CalculateHeight(vertices[i].x, vertices[i].z) * power;
        }

        mf.mesh.vertices = vertices;
    }

    float CalculateHeight(float x, float y)
    {
        float xCord = x * scale + xOffset;
        float yCord = y * scale + yOffset;

        return Mathf.PerlinNoise(xCord, yCord);
    }


   
}
    