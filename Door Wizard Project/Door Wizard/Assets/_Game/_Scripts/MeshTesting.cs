﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshTesting : MonoBehaviour
{
    [SerializeField] Mesh mesh;
    void Update()
    {
        mesh.RecalculateBounds();
    }
}
