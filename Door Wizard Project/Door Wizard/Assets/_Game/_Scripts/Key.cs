﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key : MonoBehaviour
{
    public KeyTypes keyType = 0;
    public GameObject keyBubble = null;
    public bool isFinalKey = false;
    Color keyColor;

    private void OnEnable()
    {
        EventManager.StartListening(Events.RegisterObjects, OnRegisterObjects);
        keyColor = GetComponentInChildren<Renderer>(true).material.GetColor("_BaseColor");
    }

    private void OnDisable()
    {
        EventManager.StopListening(Events.RegisterObjects, OnRegisterObjects);
    }

    private void Update()
    {
        
        if (keyType == KeyTypes.Green)
        {
            if (UIManager.Instance.ghostKeyAmt == 5)
            {
                keyBubble.SetActive(false);
            }
        }
 
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerManager.Instance.PlayerLayerInt || other.gameObject.layer == LayerManager.Instance.GhostModeLayerInt)
        {
            if (keyType == KeyTypes.Ghost)
            {
                Debug.Log("get ghost key");
                EventManager.TriggerEvent(Events.GetGhostKey);
            }

            AudioManager.Instance.PlaySFXClip(SFXClips.GetKey);

            //get key
            KeyManager.Data data = new KeyManager.Data
            {
                currentKeyType = keyType,
                currentKeyColor = keyColor
            };
            EventManager.TriggerEvent(Events.GetKey, data);

            //unlock door
            EventManager.TriggerEvent(Events.UnlockDoor, data);

            gameObject.SetActive(false);
         
           
        }

    }

    void OnRegisterObjects()
    {
        KeyManager.Instance.RegisterKey(this);

        if (isFinalKey)
        {
            PuzzleManager.Instance.RegisterFinalKey(this);
            gameObject.SetActive(false);
        }
    }


}
