﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Lean.Touch;

public class ChooseAndPlaceDoor : MonoBehaviour
{
   
    public float range;
    public float manaCost = 5f;
    Vector3 mouseStartPos;
  
    public LayerMask layerMask;
    public Camera cam;
    public Material cannotPlaceMat;
    Ray ray;
    RaycastHit hit;
    bool doSpawnDoor = false;

    [Header("Portal Doors")]
    public int maxPortals = 2;
    public List<TwoWayPortalDoor> spawnedDoors = new List<TwoWayPortalDoor>();
    public List<PreviewDoor> previewDoors = new List<PreviewDoor>();
    public List<GhostDoor> ghostDoors = new List<GhostDoor>();

    private void OnEnable()
    {
        EventManager.StartListening(Events.LoadLevel, OnLoadLevel);
        EventManager.StartListening(Events.ClearPortalDoors, OnClearPortalDoors);

        
        LeanTouch.OnFingerDown += HandleFingerDown;
        LeanTouch.OnFingerUp += HandleFingerUp;
        LeanTouch.OnFingerUpdate += HandleFingerUpdate;

    }

    private void OnDisable()
    {
        EventManager.StopListening(Events.LoadLevel, OnLoadLevel);
        EventManager.StopListening(Events.ClearPortalDoors, OnClearPortalDoors);
        
        LeanTouch.OnFingerDown -= HandleFingerDown;
        LeanTouch.OnFingerUp -= HandleFingerUp;
        LeanTouch.OnFingerUpdate -= HandleFingerUpdate;
    }

    private void Start()
    {
        OnClearPortalDoors();

        cam = CameraControls.Instance.GetCurrentCamera();
    }
   
    public void SpawnPreview(GameObject currentPreviewPortal)
    {
        if (GameManager.Instance.isPaused == false && CameraControls.Instance.playMode)
        {
            Vector3 spawnPos = hit.point;
            spawnPos.y += currentPreviewPortal.transform.position.y;

            //spawn door
            GameObject newSpawnedPortal = Instantiate(currentPreviewPortal, spawnPos, currentPreviewPortal.transform.rotation);

            PreviewDoor newPreview = newSpawnedPortal.GetComponent<PreviewDoor>();
            AddPortalPreview(newPreview);
            SpawnDoorManager.Instance.isPreviewActive = true;

        }
    }


    public void SpawnDoor(GameObject currentPortal)
    {
        if (GameManager.Instance.isPaused == false && CameraControls.Instance.playMode)
        {
            //if it's not a ghost door
            if (SpawnDoorManager.Instance.GetCurrentDoorType() != DoorTypes.GhostPortal)
            {
                Vector3 spawnPos = hit.point;
                spawnPos.y += currentPortal.transform.position.y;

                //spawn door
                GameObject newSpawnedPortal = Instantiate(currentPortal, spawnPos, previewDoors[0].transform.rotation); // currentPortal.transform.rotation

                //disable preview
                SpawnDoorManager.Instance.isPreviewActive = false;
                ParticlesManager.Instance.InstantiateParticle(Particles.PlaceDoor, spawnPos, newSpawnedPortal.transform);
                AudioManager.Instance.PlaySFXClip(SFXClips.PlaceDoor);
                
                //decrease mana
                StatsManager.Instance.AddMana(-manaCost);

                //save portal to list
                TwoWayPortalDoor newPortal = newSpawnedPortal.GetComponent<TwoWayPortalDoor>();
                AddPortalDoor(newPortal);
            }
           
            //if is ghost door
            else
            {
              
                Vector3 spawnPos = hit.point;
                spawnPos.y += currentPortal.transform.position.y;

                //spawn door
                GameObject newSpawnedGhostPortal = Instantiate(currentPortal, spawnPos, previewDoors[0].transform.rotation);
                ParticlesManager.Instance.InstantiateParticle(Particles.PlaceDoor, spawnPos, newSpawnedGhostPortal.transform);
                AudioManager.Instance.PlaySFXClip(SFXClips.PlaceDoor);

                //disable preview
                SpawnDoorManager.Instance.isPreviewActive = false;
                //decrease mana
                StatsManager.Instance.AddMana(-manaCost);

                //save portal to portal manager
                GhostDoor newGhostPortal = newSpawnedGhostPortal.GetComponent<GhostDoor>();
                AddGhostDoor(newGhostPortal);

            }

        }
    }

    public void AddPortalDoor(TwoWayPortalDoor newPortalDoor)
    {
        if (spawnedDoors.Count < maxPortals)
        {
            spawnedDoors.Add(newPortalDoor);
        }
        //if maxed out, replace the last index with new portal
        else
        {
            Destroy(spawnedDoors[0].gameObject);
            spawnedDoors.RemoveAt(0);
            spawnedDoors.Add(newPortalDoor);
        }

        LinkPortals(spawnedDoors[0], newPortalDoor);
    } 
    
    public void AddGhostDoor(GhostDoor newGhostDoor)
    {
        if (ghostDoors.Count == 0)
        {
            ghostDoors.Add(newGhostDoor);
        }
        //if maxed out, replace the last index with new portal
        else
        {
            if (ghostDoors.Count > 0)
            {
                Destroy(ghostDoors[0].gameObject);
                ghostDoors.RemoveAt(0);
            }
            ghostDoors.Add(newGhostDoor);
        }
    } 
    
    
    
    public void AddPortalPreview(PreviewDoor newPortalPreview)
    {
        if (previewDoors.Count == 0)
        {
            previewDoors.Add(newPortalPreview);
        }
       // if maxed out, replace the last index with new preview
        else
        {
            if (previewDoors.Count > 0)
            {
                OnClearPortalPreviews();
            }
            previewDoors.Add(newPortalPreview);
        }
    }



    void LinkPortals(TwoWayPortalDoor portal1, TwoWayPortalDoor portal2)
    {
        //reference portals
        portal1.exitPortal = portal2;
        portal2.exitPortal = portal1;

        //unlock doors
        portal1.isLocked = false;
        portal2.isLocked = false;

        //if one of the doors are open, open the other one too
        if (portal1.isOpen)
        {
            portal1.GetExitDoor();
            portal2.GetExitDoor();
            portal2.Open(portal2.door);
        }
    }

    #region LEAN TOUCH
    //void HandleFingerTap(LeanFinger finger)
    //{
    //    ////LEAN TOUCH SPAWN DOOR
    //    //if (finger.Tap)
    //    //{
    //    //    mouseStartPos = finger.ScreenPosition;
    //    //}

    //    //if ui is not active
    //    if (!UIManager.Instance.IsScreenOpen(UIScreens.DoorSelectScreen) && CameraControls.Instance.IsControlEnabled())
    //    {
    //        //the ray doesn't work: cam.screenpointtoray it doesn't like it
    //        //ray = finger.GetRay(cam);

    //        //check if ray is hitting ground
    //        ray = finger.GetStartRay(cam);
    //        //ray = cam.ScreenPointToRay(finger.ScreenPosition);
    //        if (Physics.Raycast(ray, out hit, range, groundMask))
    //        {
    //            //check if mouse is hitting any ui
    //            if (!EventSystem.current.IsPointerOverGameObject())
    //            {
    //                //if not, enable mouse holding timer
    //                doSpawnDoor = true;
    //                CameraControls.Instance.EnableControl(false);
    //                tHoldClick = 0;
    //            }
    //        }
    //    }

    //    //if (finger.Tap)
    //    //{
    //    //    mouseStartPos = finger.ScreenPosition;


    //    //    if (doSpawnDoor && !CameraControls.Instance.IsControlEnabled())
    //    //    {
    //    //        //if click is not being held, spawn previous selected door
    //    //        if (tHoldClick < tMaxHoldClick)
    //    //        {
    //    //            SpawnDoor(SpawnDoorManager.Instance.GetCurrentDoor());
    //    //            Debug.Log("spawned with touch!");
    //    //        }

    //    //        //reset timer
    //    //        tHoldClick = 0;

    //    //        //if mouse is not over UI when released, close ui screen
    //    //        //if (!EventSystem.current.IsPointerOverGameObject())
    //    //        //{
    //    //            UIManager.Instance.CloseScreen(UIScreens.DoorSelectScreen);
    //    //        //}
    //    //    }

    //    //    doSpawnDoor = false;
    //    //}


    //}

    //TOUCH SPAWN
    void HandleFingerDown(LeanFinger finger) //spawn preview
    {
       
    }

    void HandleFingerUpdate(LeanFinger finger)
    {
        ray = finger.GetStartRay(cam);
        if (Physics.Raycast(ray, out hit, range, layerMask))
        {
            //Debug.Log(hit.transform.gameObject.layer);
            //if hits a barrier
            if (hit.transform.gameObject.layer == LayerManager.Instance.BarrierLayerInt || hit.transform.gameObject.layer == LayerManager.Instance.PlayerLayerInt)
            {
                doSpawnDoor = false;
            }
            //else
            else
            {
                if (!EventSystem.current.IsPointerOverGameObject())
                {
                    mouseStartPos = finger.ScreenPosition;
                    doSpawnDoor = true;
                    // Debug.Log(mouseStartPos);

                    if (SpawnDoorManager.Instance.isPreviewActive == false)
                    {
                        SpawnPreview(SpawnDoorManager.Instance.GetCurrentPreview()); //preview portal
                    }
                }
            }
        }
        else
        {
            // CannotPlacePreview(SpawnDoorManager.Instance.GetCurrentPreview());
        }
        //if (finger.IsOverGui == false || finger.StartedOverGui == false)
        //{

        //}
        //    //LEAN TOUCH BRING UP UI ON HOLD
        //    if (doSpawnDoor)
        {
        //        //increase timer
        //        if (tHoldClick < tMaxHoldClick)
        //        {
        //            tHoldClick += Time.deltaTime;
        //        }

        //        //if timer max is reached, open door select ui
        //        if (tHoldClick >= tMaxHoldClick && finger.ScreenPosition.x == mouseStartPos.x && finger.ScreenPosition.y == mouseStartPos.y)
        //        {
        //            if (!UIManager.Instance.IsScreenOpen(UIScreens.DoorSelectScreen))
        //            {
        //                UIManager.Instance.MoveScreenTo(UIScreens.DoorSelectScreen, mouseStartPos);
        //                UIManager.Instance.OpenScreen(UIScreens.DoorSelectScreen);
        //            }
        //        }
        //        else
        //        {
        //            UIManager.Instance.CloseScreen(UIScreens.DoorSelectScreen);
        //        }
           }
    }

    void HandleFingerUp(LeanFinger finger) //spawn portal in preview pos and rot
    {
        SpawnDoorManager.Instance.isPreviewActive = false;
        if (doSpawnDoor)
        {
            SpawnDoor(SpawnDoorManager.Instance.GetCurrentDoor());
            doSpawnDoor = false;
        }
        else
        {
            Vector3 spawnPos = hit.point;
            
            if (spawnPos != Vector3.zero && !EventSystem.current.IsPointerOverGameObject()) //if not blank shot & not over ui
            {
                ParticlesManager.Instance.InstantiateParticle(Particles.CantPlaceDoor, spawnPos, transform);
                AudioManager.Instance.PlaySFXClip(SFXClips.CantPlaceDoor);
            }
        }
        OnClearPortalPreviews();
   
    }

    #endregion

    public void OnClearPortalDoors()
    {
        for (int i = 0; i < spawnedDoors.Count; i++)
        {
            if (spawnedDoors[i] != null)
            {
                Destroy(spawnedDoors[i].gameObject);
            }
        }

        spawnedDoors.Clear();

    }
    
    public void OnClearPortalPreviews()
    {
        for (int i = 0; i < previewDoors.Count; i++)
        {
            if (previewDoors[i] != null)
            {
                Destroy(previewDoors[i].gameObject);
            }
        }

        previewDoors.Clear();
    }   
    
    public void OnClearGhostPortals()
    {
        for (int i = 0; i < ghostDoors.Count; i++)
        {
            if (ghostDoors[i] != null)
            {
                Destroy(ghostDoors[i].gameObject);
            }
        }

        ghostDoors.Clear();
    }

    void OnLoadLevel()
    {
        Start();
    }

   
}