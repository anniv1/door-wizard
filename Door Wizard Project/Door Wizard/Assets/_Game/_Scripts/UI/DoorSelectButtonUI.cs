﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DoorSelectButtonUI : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler, IPointerUpHandler
{
    public bool isEnabled = false;
    
    public DoorTypes doorType = 0;
    [SerializeField] GameObject outline = null;
    [SerializeField] GameObject lockSprite = null;

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (isEnabled)
        {
            outline.SetActive(true);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (isEnabled)
        {
            outline.SetActive(false);
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (isEnabled)
        {
            SpawnDoorManager.Instance.ChangeDoorType(doorType);
            outline.SetActive(true);
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (isEnabled)
        {

        }
    }

    public void SetLockSpriteActive(bool activeState)
    {
        lockSprite.SetActive(activeState);
    }
}
