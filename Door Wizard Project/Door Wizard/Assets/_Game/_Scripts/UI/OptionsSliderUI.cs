﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsSliderUI : MonoBehaviour
{
    Slider optionsSlider;

    private void OnEnable()
    {
        optionsSlider = GetComponent<Slider>();
    }

    public void ChangeVolume(string audioType)
    {
        if (optionsSlider != null)
        {
            switch (audioType)
            {
                case "Music":
                    AudioManager.Instance.ChangeLoopVolume(optionsSlider.value);
                    break;
                case "SFX":
                    AudioManager.Instance.ChangeSFXVolume(optionsSlider.value);
                    break;
                default:
                    break;
            }
        }
    }
}
