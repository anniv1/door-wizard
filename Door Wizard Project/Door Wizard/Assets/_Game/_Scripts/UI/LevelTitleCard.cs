﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class LevelTitleCard : MonoBehaviour
{

    Image titleImage;
    Outline titleOutline;
    Color initTitleColor;
    Color initOutlineColor;
    Color clearColor;

    float t = 0;
    bool changeColor = true;
    bool resetColor = false;
    bool fadeOut = false;
    bool resetCard = false;

    [SerializeField] GameObject instructions = null;

    private void OnEnable()
    {

        if (!resetCard)
        {
            titleImage = GetComponent<Image>();
            titleOutline = GetComponent<Outline>();
            initTitleColor = titleImage.color;
            initOutlineColor = titleOutline.effectColor;
            clearColor = titleOutline.effectColor;
            clearColor.a = 0;
        }
        else
        {
            titleOutline.effectColor = initOutlineColor;
            titleImage.color = initTitleColor;
        }
    }

    // Update is called once per frame
    void Update()
    {
        //pulsing animation
        if (changeColor)
        {
            if (t < 0.1f)
            {
                t += Time.deltaTime * 0.08f;
               
            }
            else
            {
                t = 0;
                resetColor = true;
                changeColor = false;
            }
        }
        else if (resetColor)
        {
            if (t < 0.1f)
            {
                t += Time.deltaTime * 0.08f;
              
            }
            else
            {
                t = 0;
                changeColor = true;
                resetColor = false;
            }
        }

        //fade out
        if (fadeOut)
        {
            if (t < 1f)
            {
                t += Time.deltaTime;
                titleImage.color = Color.Lerp(titleImage.color, Color.clear, t);
            }
            else
            {
                resetCard = true;
                gameObject.SetActive(false);
            }
        }
        else
        {
            //get any key down to hide
            if (Input.anyKeyDown)
            {
                GameManager.Instance.StartGame();
                if (instructions != null) { instructions.SetActive(false); }

                changeColor = false;
                resetColor = false;
                t = 0;
                fadeOut = true;
            }

            if (Input.GetMouseButtonDown(0))
            {
                GameManager.Instance.StartGame();
                if (instructions != null) { instructions.SetActive(false); }

                changeColor = false;
                resetColor = false;
                t = 0;
                fadeOut = true;
            }
        }
    }

}


