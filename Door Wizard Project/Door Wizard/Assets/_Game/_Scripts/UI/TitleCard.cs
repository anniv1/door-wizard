﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class TitleCard : MonoBehaviour
{
    public Button continueBtn = null;

    Image titleImage;
    Outline titleOutline;
    Color initTitleColor;
    Color initOutlineColor;
    Color clearColor;
    
    float t = 0;
    bool changeColor = true;
    bool resetColor = false;
    bool fadeOut = false;
    bool resetCard = false;

    [SerializeField] GameObject instructions = null;

    private void OnEnable()
    {
        if (PlayerPrefs.GetInt("FIRSTTIMEOPENING", 1) == 1)
        {           
            Debug.Log("First Time Opening");

            //Set first time opening to false
            PlayerPrefs.SetInt("FIRSTTIMEOPENING", 0);

            if (continueBtn != null)
            {
                continueBtn.interactable = false;
            }

        }
        else
        { 
            Debug.Log("NOT First Time Opening");

            if (continueBtn != null)
            {
                continueBtn.interactable = true;
            }
       
        }

        if (GameManager.Instance.StarsCollected == 0)
        {
            if (continueBtn != null)
            {
                continueBtn.interactable = false;
            }
        }
        else
        {
            if (continueBtn != null)
            {
                continueBtn.interactable = true;
            }
        }

        if (!resetCard)
        {
            titleImage = GetComponent<Image>();
            titleOutline = GetComponent<Outline>();
            initTitleColor = titleImage.color;
            initOutlineColor = titleOutline.effectColor;
            clearColor = titleOutline.effectColor;
            clearColor.a = 0;
        }
        else
        {
            titleOutline.effectColor = initOutlineColor;
            titleImage.color = initTitleColor;
        }
    }

    void Update()
    {
        //pulsing animation
        if (changeColor)
        {
            if (t < 0.1f)
            {
                t += Time.deltaTime * 0.08f;
            }
            else
            {
                t = 0;
                resetColor = true;
                changeColor = false;
            }
        }
        else if (resetColor)
        {
            if (t < 0.1f)
            {
                t += Time.deltaTime * 0.08f;
            }
            else
            {
                t = 0;
                changeColor = true;
                resetColor = false;
            }
        }

        //fade out
        if (fadeOut)
        {
            if (t < 1f)
            {
                t += Time.deltaTime;
                titleImage.color = Color.Lerp(titleImage.color, Color.clear, t);
            }
            else
            {
                resetCard = true;
                gameObject.SetActive(false);
            }
        }
        else
        {
         //moved this to the title level card script, not needed here currently!
         //here was the push any button to continue
        }
    }

    public void NewGame()
    {
       
        GameManager.Instance.NewGame();
        GameManager.Instance.StartGame();
      
        if (instructions != null) { instructions.SetActive(false); }

        changeColor = false;
        resetColor = false;
        t = 0;
        fadeOut = true;
        SceneManager.LoadSceneAsync(1);
    }

    public void ContinueGame()
    {
      
        GameManager.Instance.StartGame();
        GameManager.Instance.ContinueGame();
        if (instructions != null) { instructions.SetActive(false); }

        changeColor = false;
        resetColor = false;
        t = 0;
        fadeOut = true;
        SceneManager.LoadSceneAsync(1);
    }
}

