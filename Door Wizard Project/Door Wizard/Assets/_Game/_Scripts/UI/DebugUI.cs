﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DebugUI : MonoBehaviour
{
    [SerializeField] GameObject debugScreen = null;

    public void OpenDebugScreen()
    {
        debugScreen.SetActive(true);
    }

    public void CloseDebugScreen()
    {
        debugScreen.SetActive(false);
    }

    public void ResetLevel()
    {
        EventManager.TriggerEvent(Events.PreLoadLevel);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void LoadScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }
}
