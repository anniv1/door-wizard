﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ProgressReset : MonoBehaviour
{
    void OnProgressReset()
    {
        GameManager.Instance.StarsCollected = 0;
        PlayerPrefs.SetInt("starsCollected", GameManager.Instance.StarsCollected);
        SceneManager.LoadScene(0);
        EventManager.TriggerEvent(Events.GetStar);
    }
}
