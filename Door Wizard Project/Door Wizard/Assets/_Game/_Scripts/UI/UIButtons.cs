﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor;

public class UIButtons : MonoBehaviour
{
    public void PauseButton()
    {
        EventManager.TriggerEvent(Events.Pause);
    }

    public void ExitPauseButton()
    {
        EventManager.TriggerEvent(Events.ExitPause);
    }

    public void RestartLevelButton()
    {
        UIManager.Instance.CloseScreen(UIScreens.ResetScreen);
        EventManager.TriggerEvent(Events.PreLoadLevel);
        int sceneBuildInt = SceneManager.GetActiveScene().buildIndex;
        UIManager.Instance.LoadScene(sceneBuildInt);
        GameManager.Instance.isPaused = false;
        UIManager.Instance.ExitPause();
    }

    public void ReturnToTitleButton()
    {
        UIManager.Instance.ExitPause();
        EventManager.TriggerEvent(Events.PreLoadLevel);
        UIManager.Instance.LoadScene("MainMenuBlockout");
    }

    public void OpenScreen(GameObject screen)
    {
        screen.SetActive(true);
    }

    public void CloseScreen(GameObject screen)
    {
        screen.SetActive(false);
        CameraControls.Instance.EnableControl(true);
    }

    public void QuitGame()
    {
        #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#endif
        if (GameManager.Instance.StarsCollected > PlayerPrefs.GetInt("StarsCollected"))
        {
            //saves the amount of starts collected
            PlayerPrefs.SetInt("StarsCollected", GameManager.Instance.StarsCollected);
        }
        Application.Quit();
    }

    public void ResetProgress()
    {
        GameManager.Instance.StarsCollected = 0;
        PlayerPrefs.SetInt("starsCollected", GameManager.Instance.StarsCollected);
        SceneManager.LoadScene(0);
        EventManager.TriggerEvent(Events.GetStar);
    }

}
