﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class UIScreen : MonoBehaviour
{
    public UIScreens screenID = 0;
    [SerializeField] List<Button> gameMenuButtons = new List<Button>();

    public void ShowGameMenuButtons()
    {
        for (int i = 0; i < gameMenuButtons.Count; i++)
        {
            gameMenuButtons[i].gameObject.SetActive(true);
        }
    }

    public void HideGameMenuButtons()
    {
        for (int i = 0; i < gameMenuButtons.Count; i++)
        {
            gameMenuButtons[i].gameObject.SetActive(false);
        }
    }
}
