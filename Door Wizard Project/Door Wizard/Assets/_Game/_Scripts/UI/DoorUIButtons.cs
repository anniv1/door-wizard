﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorUIButtons : MonoBehaviour
{
    private void OnEnable()
    {
        EventManager.StartListening(Events.StartConversation, StartConversation);
        EventManager.StartListening(Events.DontStartConversation, DontStartConversation);
    }
      
    private void OnDisable()
    {
        EventManager.StartListening(Events.StartConversation, StartConversation);
        EventManager.StartListening(Events.DontStartConversation, DontStartConversation);
    }


    public void InteractButton()
    {
        //opens doors
        GameManager.Instance.isDoorOpen = true;

        //if you can interact
        if (GameManager.Instance.canInteract == true)
        {
            Debug.Log("star convo");
            if (GameManager.Instance.isTalking == true)
            {
                EventManager.TriggerEvent(Events.DontStartConversation);
                Debug.Log("star convo");
            }
            else
            {
                EventManager.TriggerEvent(Events.StartConversation);
                Debug.Log("end convo");
            }
        }
    }

    public void ClearSpawnDoors()
    {
      
        EventManager.TriggerEvent(Events.ClearPortalDoors);
        Debug.Log("doors cleared");

    }

    //currently this preloads the button so the dialogue will start when you walk close- not meant to do that but Imma leave it for now
    void StartConversation()
    {
        GameManager.Instance.isTalking = true;
    }   
    
    void DontStartConversation()
    {
        GameManager.Instance.isTalking = false;
    }
  
}
