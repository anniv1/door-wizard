﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoorSelect : MonoBehaviour
{
    public Animation anim;

    public AnimationClip openSelect;
    public AnimationClip closeSelect;

    public GameObject oppositeBtn;


    public void OpenDoorSelectBtn()
    {
        anim.Play(openSelect.name);
        oppositeBtn.SetActive(true);
        gameObject.SetActive(false);
      
      
    }

    public void CloseDoorSelectBtn()
    {
        anim.Play(closeSelect.name);
        oppositeBtn.SetActive(true);
        gameObject.SetActive(false);
       
       
    }
}
