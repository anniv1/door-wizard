﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//UI ENUMS: UIManager.cs
//AUDIO ENUMS: AudioManager.cs
//PARTICLE ENUMS: ParticleManager.cs

public enum Scenes
{
    MainMenu,
    Level1,
    Level2,
    PrizeRoom,
    Tea1,
    Spooky,
    COUNT,
}

public enum Segments
{
    None,
    One,
    Two,
    Three,
    Four,
    Five,
    //COUNT
}

public enum KeyTypes
{
    None,//0
    Blue,//1
    Red,//2
    Green,//3
    Yellow,//4
    Purple,//5
    Ghost,//6
    COUNT
}

public enum DoorTypes
{
    None, //0
    PlayerPortal, //1
    ObjectPortal, //2
    GhostPortal,//3
    Green, //4
    Yellow, //5
    COUNT
}

public enum PreviewDoorTypes
{
    None, //0
    PlayerPreview, //1
    ObjectPreivew, //2
    GhostPreview, //3
    COUNT
}

public enum Directions
{
    Up,
    UpRight,
    Right,
    DownRight,
    Down,
    DownLeft,
    Left,
    UpLeft,
    COUNT
}
