﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSparkles : MonoBehaviour
{
    ParticleSystem sparkles;
    ParticleSystem.MinMaxCurve initStartSize = 0;
    
    string restartParticlesDelay = "RestartParticlesDelay";
    bool doInteract = false;

    private void Awake()
    {
        sparkles = GetComponentInChildren<ParticleSystem>();
        initStartSize = sparkles.main.startSize;
    }


    public void DisableInteraction()
    {
        sparkles.Stop();
        enabled = false;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == LayerManager.Instance.PlayerLayerInt || collision.gameObject.layer == LayerManager.Instance.GhostModeLayerInt)
        {
            StopSparkles();
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.layer == LayerManager.Instance.PlayerLayerInt || collision.gameObject.layer == LayerManager.Instance.GhostModeLayerInt)
        {
            doInteract = false;
            StartCoroutine(restartParticlesDelay);
        }
    }

    public void StopSparkles()
    {
        sparkles.Stop();
        sparkles.Clear();
    }

    public void OnObjectInteraction()
    {
        doInteract = true;
        StartCoroutine(restartParticlesDelay);
    }

    IEnumerator RestartParticlesDelay()
    {
        if (doInteract)
        {
            sparkles.Stop();
            doInteract = false;
        }

        yield return new WaitForSeconds(0.5f);

        sparkles.Play();
    }
}
