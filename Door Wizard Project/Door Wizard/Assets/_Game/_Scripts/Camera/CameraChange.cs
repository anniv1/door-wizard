﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraChange : MonoBehaviour
{
    public float tempNearClipPlane = 0f;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerManager.Instance.PlayerLayerInt)
        {
            EventManager.TriggerEvent(Events.ModifyCameraFollow, this);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == LayerManager.Instance.PlayerLayerInt)
        {
            EventManager.TriggerEvent(Events.ResetCameraFollow);
        }

    }
}
