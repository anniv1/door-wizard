﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HiddenByCamera : MonoBehaviour
{
    MeshRenderer meshRend;

    private void OnEnable()
    {
        OnRegisterObjects();
    }
    private void Awake()
    {
        meshRend = GetComponent<MeshRenderer>();
    }

    void OnRegisterObjects()
    {
        CameraControls.Instance.RegisterHiddenObject(meshRend);
    }
}
