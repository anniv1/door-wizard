﻿using UnityEngine;
using System.Collections;

public class CamFollow : MonoBehaviour
{
    [HideInInspector] public GameObject player; //Public variable to store a reference to the player game object
    public bool defaultSettings = true;
    public bool cameraIsOrtho = false;
    bool doChange = false;
    bool doReset = false;

    Vector3 offset;            //Private variable to store the offset distance between the player and camera
    float nearClippingOffsetMultiplier = 1.5f;
    Vector3 initPos;
    float initNearClipPlane;
    float moddedNearClipPlane;
    float nearClippingOffset;
    Vector3 velocity = Vector3.zero;
    float t;

    [HideInInspector] public Camera cam;

    private void OnEnable()
    {
        EventManager.StartListening(Events.RegisterObjects, OnRegisterObjects);
        EventManager.StartListening(Events.LoadLevel, OnLoadLevel);
        EventManager.StartListening(Events.Pause, OnPause);
        EventManager.StartListening(Events.ExitPause, ExitPause);
        EventManager.StartListening(Events.ModifyCameraFollow, OnModifyCameraFollow);
        EventManager.StartListening(Events.ResetCameraFollow, OnResetCameraFollow);
    }

    private void OnDisable()
    {
        EventManager.StartListening(Events.RegisterObjects, OnRegisterObjects);
        EventManager.StopListening(Events.LoadLevel, OnLoadLevel);
        EventManager.StopListening(Events.Pause, OnPause);
        EventManager.StopListening(Events.ExitPause, ExitPause);
        EventManager.StopListening(Events.ModifyCameraFollow, OnModifyCameraFollow);
        EventManager.StopListening(Events.ResetCameraFollow, OnResetCameraFollow);
    }

    private void Update()
    {
        if (player != null)
        {
            transform.position = Vector3.SmoothDamp(transform.position, player.transform.position + offset, ref velocity, 0.5f);

            if (cameraIsOrtho)
            {
                if (CameraControls.Instance.playMode)
                {
                    if (defaultSettings)
                    {
                        //near clipping offset
                        if (initPos.y - transform.position.y > 0)
                        {
                            nearClippingOffset = Mathf.Abs(initPos.y - transform.position.y) * nearClippingOffsetMultiplier;          
                        }
                        else
                        {
                            nearClippingOffset = initNearClipPlane;
                        }
                        cam.nearClipPlane = initNearClipPlane - nearClippingOffset;
                    }
                    else
                    {
                        if (!doChange && !doReset)
                        {
                            cam.nearClipPlane = moddedNearClipPlane;
                        }
                    }
                }

                ChangeLocalCameraSettings();
                ResetLocalCameraSettings();
            }
        }
    }
    public void SetDefaultPlayerOffset()
    {
        //Calculate and store the offset value by getting the distance between the player's position and camera's position.
        offset = transform.position - player.transform.position;
    }

    public void OnResetCameraFollow()
    {
        t = 0;

        doChange = false;
        doReset = true;
    }

    public void OnModifyCameraFollow()
    {
        defaultSettings = false;
        t = 0;

        CameraChange data = EventManager.GetTriggerData<CameraChange>();
        if (data != null)
        {
            moddedNearClipPlane = data.tempNearClipPlane;
        }

        doReset = false;
        doChange = true;
    }

    void ChangeLocalCameraSettings()
    {
        if (doChange)
        {
            if (t < 1f)
            {
                t += Time.deltaTime;
                cam.nearClipPlane = Mathf.Lerp(cam.nearClipPlane, moddedNearClipPlane, t);
            }
            else
            {
                doChange = false;
            }
        }
    }

    void ResetLocalCameraSettings()
    {
        if (doReset)
        {
            if (t < 1f)
            {
                t += Time.deltaTime;
                nearClippingOffset = (initPos.y - transform.position.y) * nearClippingOffsetMultiplier;
                cam.nearClipPlane = Mathf.Lerp(cam.nearClipPlane, initNearClipPlane - nearClippingOffset, t);
            }
            else
            {
                defaultSettings = true;
                doReset = false;
            }
        }
    }

    void OnRegisterObjects()
    {
        cam = GetComponent<Camera>();
        initPos = transform.position;
        initNearClipPlane = cam.nearClipPlane;

        CameraControls.Instance.RegisterCamera(this);
    }

    void OnLoadLevel()
    {
        CameraControls.Instance.RegisterCamera(this);
    }
    void OnPause()
    {

    }
    void ExitPause()
    {

    }
}

