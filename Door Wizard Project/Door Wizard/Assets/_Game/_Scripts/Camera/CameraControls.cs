﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Touch;
using UnityEngine.EventSystems;

//mouse pan code: https://www.youtube.com/watch?v=4_HUlAFlxwU&ab_channel=PressStart

public class CameraControls : Singleton<CameraControls>
{
    [HideInInspector] public Camera currentCamera = null;
    public bool playMode = true;
    [SerializeField] float zoomScale = 0;
    [SerializeField] float zoomMinZ = 0;
    [SerializeField] float zoomMaxZ = 0;
    [SerializeField] float panScale = 0;
    [SerializeField] List<MeshRenderer> hiddenObjects = new List<MeshRenderer>();

    float pinchScale = 0;

    Vector3 mouseStartPos;
    Vector3 fingerStartPos;
    Vector2 fingerStartScreenPos;

    private Vector3 mouseStart;
    public float groundZ = 0;
    CamFollow camFollow;

    [HideInInspector] public bool doControl = true;
    Vector3 initCameraPos;
    float initZoom;
    float initZoomScale;
    float zoomScaleMultiplier = 1.06f;
    float initFarClipPlane;
    float zoomFarClipPlane;
    float farClipOffsetMultiplier = 1.2f;
    float t;
    bool resetZoom = false;

    Ray ray;
    RaycastHit hit;
    float range = 300f;

    //LEAN TOUCH
    float tFingerDown = 0f;

    private void OnEnable()
    {
        EventManager.StartListening(Events.PreLoadLevel, OnPreLoadLevel);
        EventManager.StartListening(Events.LoadLevel, OnLoadLevel);

        LeanTouch.OnFingerUpdate += HandleFingerUpdate;
        LeanTouch.OnFingerUp += HandleFingerUp;
        LeanTouch.OnFingerDown += HandleFingerDown;
    }

    private void OnDisable()
    {
        EventManager.StopListening(Events.PreLoadLevel, OnPreLoadLevel);
        EventManager.StopListening(Events.LoadLevel, OnLoadLevel);
        LeanTouch.OnFingerUpdate -= HandleFingerUpdate;
        LeanTouch.OnFingerUp -= HandleFingerUp;
        LeanTouch.OnFingerDown -= HandleFingerDown;
    }

    void Start()
    {
        initZoomScale = zoomScale;
    }

    void Update()
    {
        if (GameManager.Instance.isPaused == false)
        {
            if (doControl && currentCamera != null)
            {
                
                //PINCH ZOOM
                if (LeanGesture.GetPinchScale(pinchScale) > 1)
                {
                    ZoomIn();
                }

                if (LeanGesture.GetPinchScale(pinchScale) < 1)
                {
                    ZoomOut();
                }

                //MOUSE ZOOM
                if (Input.mouseScrollDelta.y > 0)
                {
                    ZoomIn();
                }

                if (Input.mouseScrollDelta.y < 0)
                {
                    ZoomOut();
                }

                //MOUSE PAN: fully functional
                if (Input.GetMouseButtonDown(0))
                {
                    mouseStart = GetWorldPosition(groundZ, Input.mousePosition);
                }
                if (Input.GetMouseButton(0))
                {
                   //Pan();
                }
                if (Input.GetMouseButtonUp(0))
                {
                    
                }
            }
        }

        ResetZoom();
    }

    void ZoomIn()
    {
        if (currentCamera.orthographicSize > zoomMinZ)
        {
            ShowCameraControls();
            ShowHiddenObjects(true);
            playMode = false;

            currentCamera.orthographicSize -= zoomScale;
            currentCamera.nearClipPlane += zoomScale * zoomScaleMultiplier;
            currentCamera.farClipPlane -= zoomScale;
            zoomFarClipPlane = currentCamera.farClipPlane;
            zoomScale /= zoomScaleMultiplier;
        }
    }

    void ZoomOut()
    {
        if (currentCamera.orthographicSize < zoomMaxZ)
        {
            ShowCameraControls();
            ShowHiddenObjects(true);
            playMode = false;

            currentCamera.orthographicSize += zoomScale;
            currentCamera.nearClipPlane -= zoomScale * zoomScaleMultiplier;
            currentCamera.farClipPlane += zoomScale;
            zoomFarClipPlane = currentCamera.farClipPlane;
            zoomScale *= zoomScaleMultiplier;
        }
    }

    private Vector3 GetWorldPosition(float z, Vector3 inputPos)
    {
        Ray mousePos = currentCamera.ScreenPointToRay(inputPos);
        Plane ground = new Plane(Vector3.forward, new Vector3(0, 0, z));

        float distance;
        ground.Raycast(mousePos, out distance);
        return mousePos.GetPoint(distance);
    }

    public void RegisterCamera(CamFollow newCamFollow)
    {
        camFollow = newCamFollow;
        currentCamera = newCamFollow.cam;
        camFollow.cameraIsOrtho = currentCamera.orthographic ? true : false;

        initCameraPos = currentCamera.transform.position;
        initZoom = currentCamera.orthographicSize;
        initFarClipPlane = currentCamera.farClipPlane;
        zoomFarClipPlane = initFarClipPlane;
    }

    public void UnregisterCamera()
    {
        camFollow.player = null;
        camFollow = null;
        currentCamera = null;
    }

    public void RegisterPlayer(PlayerMovement newPlayer)
    {
        camFollow.player = newPlayer.gameObject;
        camFollow.SetDefaultPlayerOffset();
    }

    public void RegisterHiddenObject(MeshRenderer newHiddenObject)
    {
        hiddenObjects.Add(newHiddenObject);
        newHiddenObject.enabled = false;
    }

    public void UnregisterAllHiddenObjects()
    {
        hiddenObjects.Clear();
    }

    public Camera GetCurrentCamera()
    {
        return currentCamera;
    }

    void ShowCameraControls()
    {
        resetZoom = false;
        camFollow.enabled = false;
        UIManager.Instance.OpenScreen(UIScreens.CameraControlScreen);
    }

    void ShowHiddenObjects(bool doShow)
    {
        if (doShow)
        {
            for (int i = 0; i < hiddenObjects.Count; i++)
            {
                hiddenObjects[i].enabled = true;
            }
        }
        else
        {
            for (int i = 0; i < hiddenObjects.Count; i++)
            {
                hiddenObjects[i].enabled = false;
            }
        }
    }

    public void EnableControl(bool doEnable)
    {
        doControl = doEnable;
    }

    public bool IsControlEnabled()
    {
        if (doControl)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool IsCamFollowEnabled()
    {
        if (camFollow != null && camFollow.enabled)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void ResetCamera()
    {
        if (camFollow != null)
        {
            camFollow.enabled = true;
            ShowHiddenObjects(false);
            UIManager.Instance.CloseScreen(UIScreens.CameraControlScreen);
            doControl = true;
            t = 0;
            playMode = true;
            if (camFollow.defaultSettings)
            {
                camFollow.defaultSettings = false;
                EventManager.TriggerEvent(Events.ResetCameraFollow);
            }
            else
            {
                EventManager.TriggerEvent(Events.ModifyCameraFollow);
            }
            resetZoom = true;
        }
    }

    private void ResetZoom()
    {
        if (resetZoom)
        {
            if (t < 1f)
            {
                t += Time.deltaTime;
                currentCamera.orthographicSize = Mathf.Lerp(currentCamera.orthographicSize, initZoom, t);
                currentCamera.farClipPlane = Mathf.Lerp(currentCamera.farClipPlane, initFarClipPlane, t);
                zoomFarClipPlane = Mathf.Lerp(zoomFarClipPlane, initFarClipPlane, t);
                zoomScale = Mathf.Lerp(zoomScale, initZoomScale, t);
            }
            else
            {
                resetZoom = false;
            }
        }
    }

    // LEAN TOUCH
    void HandleFingerDown(LeanFinger finger)
    {
        if (doControl)
        {
            fingerStartScreenPos = finger.ScreenPosition;
            fingerStartPos = GetWorldPosition(groundZ, fingerStartScreenPos);
            tFingerDown = 0;
        }
    }

    void HandleFingerUpdate(LeanFinger finger)
    {
        tFingerDown += Time.deltaTime;
        if (tFingerDown > 0.15f)
        {
            if (doControl && LeanTouch.Fingers.Count == 1)
            {
                //if mouse position is more than "very slightly" off starting position
                if (fingerStartScreenPos != finger.ScreenPosition)
                {
                    if (!EventSystem.current.IsPointerOverGameObject() && finger.StartedOverGui == false)
                    {
                        ray = finger.GetStartRay(currentCamera);
                        ShowCameraControls();
                        ShowHiddenObjects(true);
                        playMode = false;

                        Vector3 direction = fingerStartPos - GetWorldPosition(groundZ, finger.ScreenPosition);
                        direction.z = direction.y * 0.5f;
                        direction.y *= 0.5f;
                        currentCamera.transform.position += direction;

                        float farClipOffset = Mathf.Abs((currentCamera.transform.position.z - initCameraPos.z) * farClipOffsetMultiplier);
                        currentCamera.farClipPlane = zoomFarClipPlane + farClipOffset;
                    }
                }
            }
        }
    }

    void HandleFingerUp(LeanFinger finger)
    {
        tFingerDown = 0f;
    }

    void OnPreLoadLevel()
    {
        UnregisterCamera();
        UnregisterAllHiddenObjects();
    }

    void OnLoadLevel()
    {
        Start();
    }
}



