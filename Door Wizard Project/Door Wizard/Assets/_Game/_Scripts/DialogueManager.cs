﻿using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Yarn;
using Yarn.Unity;

public class DialogueManager : Singleton<DialogueManager>
{ 
    public DialogueRunner dialogueRunner;
    public List<string> dialogueNodes = new List<string>();

    private void OnEnable()
    {
        EventManager.StartListening(Events.RunDialogue, RunDialogue);
    }

    private void OnDisable()
    {
        EventManager.StopListening(Events.RunDialogue, RunDialogue);
    }

    //DIALOGUE
    void RunDialogue()
    {

    }

    public void OnDialogueEnd()
    {
        GameManager.Instance.playerCanMove = true;
    }
}
