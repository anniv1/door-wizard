﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LayerManager : Singleton<LayerManager>
{
    [SerializeField] string playerLayer = "";
    public int PlayerLayerInt
    {
        get;
        internal set;
    }

    [SerializeField] string groundLayer = "";
    public int GroundLayerInt
    {
        get;
        internal set;
    }

    [SerializeField] string barrierLayer = "";
    public int BarrierLayerInt
    {
        get;
        internal set;
    }

    [SerializeField] string shogiLayer = "";
    public int ShogiLayerInt
    {
        get;
        internal set;
    }

    [SerializeField] string cameraChangeLayer = "";
    public int CameraChangeLayerInt
    {
        get;
        internal set;
    } 
    
    [SerializeField] string npcLayer = "";
    public int NPCLayerInt
    {
        get;
        internal set;
    } 
    
    [SerializeField] string pushBlockLayer = "";
    public int PushBlockLayerInt
    {
        get;
        internal set;
    } 
    [SerializeField] string terrainLayer = "";
    public int TerrainLayerInt
    {
        get;
        internal set;
    }
    
    [SerializeField] string starLayer = "";
    public int StarLayerInt
    {
        get;
        internal set;
    } 
    
    [SerializeField] string ghostModeLayer = "";
    public int GhostModeLayerInt
    {
        get;
        internal set;
    }

    [SerializeField] string waterLayer = "";
    public int WaterLayerInt
    {
        get;
        internal set;
    }

    protected override void OnAwake()
    {
        PlayerLayerInt = LayerMask.NameToLayer(playerLayer);
        NPCLayerInt = LayerMask.NameToLayer(npcLayer);
        GroundLayerInt = LayerMask.NameToLayer(groundLayer);
        BarrierLayerInt = LayerMask.NameToLayer(barrierLayer);
        ShogiLayerInt = LayerMask.NameToLayer(shogiLayer);
        CameraChangeLayerInt = LayerMask.NameToLayer(cameraChangeLayer);
        PushBlockLayerInt = LayerMask.NameToLayer(pushBlockLayer);
        TerrainLayerInt = LayerMask.NameToLayer(terrainLayer);
        StarLayerInt = LayerMask.NameToLayer(starLayer);
        GhostModeLayerInt = LayerMask.NameToLayer(ghostModeLayer);
        WaterLayerInt = LayerMask.NameToLayer(waterLayer);
    }

   
}
