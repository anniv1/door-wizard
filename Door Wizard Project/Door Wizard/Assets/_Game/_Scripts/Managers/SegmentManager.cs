﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SegmentManager : Singleton<SegmentManager>
{
    public List<Segment> segments = new List<Segment>();
    public List<SegmentBranch> allSegmentBranches = new List<SegmentBranch>();
    public List<TwoWayPortalDoor> allSegmentPortals = new List<TwoWayPortalDoor>();
    Dictionary<Segments, Segment> segmentsDict = new Dictionary<Segments, Segment>();
    Dictionary<TwoWayPortalDoor, SegmentBranch> portalsDict = new Dictionary<TwoWayPortalDoor, SegmentBranch>();
    Dictionary<SegmentBranch, bool> branchesDict = new Dictionary<SegmentBranch, bool>();

    private void OnEnable()
    {
        EventManager.StartListening(Events.PreLoadLevel, OnPreLoadLevel);
    }

    private void OnDisable()
    {
        EventManager.StopListening(Events.PreLoadLevel, OnPreLoadLevel);
    }

    public void RegisterSegment(Segment newSegment)
    {
        segments.Add(newSegment);
        segmentsDict.Add(newSegment.segmentID, newSegment);
    }

    public void RegisterSegmentPortal(TwoWayPortalDoor newSegmentPortal)
    {
        allSegmentPortals.Add(newSegmentPortal);
    }

    public void RegisterSegmentBranch(SegmentBranch newSegmentBranch)
    {
        allSegmentBranches.Add(newSegmentBranch);
        branchesDict.Add(newSegmentBranch, false);
        for (int i = 0; i < newSegmentBranch.segmentPortals.Count; i++)
        {
            portalsDict.Add(newSegmentBranch.segmentPortals[i], newSegmentBranch);
        }
    }

    void UnregisterAllSegments()
    {
        segments.Clear();
        allSegmentBranches.Clear();
        allSegmentPortals.Clear();
        segmentsDict.Clear();
        portalsDict.Clear();
        branchesDict.Clear();
    }

    public TwoWayPortalDoor GetSegmentPortal(Segments segmentID)
    {
        //get segment from dictionary
        Segment currentSegment = segmentsDict[segmentID];
        List<SegmentBranch> currentSegmentBranches = currentSegment.segmentBranches.ToArray().ToList();

        //get random portal from segment
        int randomBranchInt = Random.Range(0, currentSegmentBranches.Count);
        SegmentBranch randomBranch = currentSegmentBranches[randomBranchInt];
        if (branchesDict[randomBranch])
        {
            currentSegmentBranches.Remove(randomBranch);
            randomBranchInt = Random.Range(0, currentSegmentBranches.Count);
            randomBranch = currentSegmentBranches[randomBranchInt];
        }
        branchesDict[randomBranch] = true;
        int randomPortalInt = Random.Range(0, randomBranch.segmentPortals.Count);

        return randomBranch.segmentPortals[randomPortalInt];
    }

    public TwoWayPortalDoor GetRandomPortal()
    {
        int randomPortalInt = Random.Range(0, allSegmentPortals.Count);
        return allSegmentPortals[randomPortalInt];
    }

    public void SetSegmentBranchInUse(SegmentBranch currentBranch)
    {
        branchesDict[currentBranch] = true;
    }

    public void SetSegmentBranchInUse(TwoWayPortalDoor currentPortal)
    {
        //get the branch connected to the portal in the dictionary
        SegmentBranch currentBranch = portalsDict[currentPortal];
        branchesDict[currentBranch] = true;
    }

    public void DeactivateBranchesNotInUse()
    {
        for (int i = 0; i < allSegmentBranches.Count; i++)
        {
            if (branchesDict[allSegmentBranches[i]] == false)
            {
                allSegmentBranches[i].gameObject.SetActive(false);
            }
        }
    }

    void OnPreLoadLevel()
    {
        UnregisterAllSegments();
    }
}
