﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//to register and unregister all doors in the current scene. clears data when moving from one scene to the other
public class DoorManager : Singleton<DoorManager>
{
    [SerializeField] Door finalDoor;
    [SerializeField] List<GameObject> allDoors = new List<GameObject>(); //just to visualize the dictionary
    Dictionary<GameObject, GameObject> twoWayDoorsDict = new Dictionary<GameObject, GameObject>();

    protected override void OnAwake()
    {
        UnregisterAllDoors();
    }

    private void OnEnable()
    {
        EventManager.StartListening(Events.LoadLevel, OnLoadLevel);
        EventManager.StartListening(Events.PreLoadLevel, OnPreLoadLevel);
        
    }

    private void OnDisable()
    {
        EventManager.StopListening(Events.LoadLevel, OnLoadLevel);
        EventManager.StopListening(Events.PreLoadLevel, OnPreLoadLevel);
    }

    void Start()
    {
        ResetDoors();
    }

    public void RegisterDoor(Door newDoor)
    {
        allDoors.Add(newDoor.gameObject);
    }

    public void RegisterDoor(TwoWayPortalDoor newTwoWayDoor)
    {
        allDoors.Add(newTwoWayDoor.gameObject);
        twoWayDoorsDict.Add(newTwoWayDoor.gameObject, newTwoWayDoor.exitPortal.gameObject);
    }

    public void RegisterFinalDoor(Door newFinalDoor)
    {
        finalDoor = newFinalDoor;
    }

    void UnregisterAllDoors()
    {
        allDoors.Clear();
        twoWayDoorsDict.Clear();
    }

    void ResetDoors()
    {
        for (int i = 0; i < allDoors.Count; i++)
        {
            twoWayDoorsDict[allDoors[i].gameObject] = null;
        }
    }

    void OnPreLoadLevel()
    {
        OnAwake();
    }

    void OnLoadLevel()
    {
        Start();
    }

}