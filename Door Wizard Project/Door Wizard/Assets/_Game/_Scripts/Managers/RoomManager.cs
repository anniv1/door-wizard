﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RoomManager : Singleton<RoomManager>
{
    public List<Room> activeRooms = new List<Room>();
    [SerializeField] Room currentRoom = null;

    List<Room> rooms = new List<Room>();

    public bool isInRoom = false;

    protected override void OnAwake()
    {
        UnregisterAllRooms();
    }

    private void OnEnable()
    {
        EventManager.StartListening(Events.PreLoadLevel, OnPreLoadLevel);
        EventManager.StartListening(Events.LoadLevel, OnLoadLevel);

        isInRoom = false;
    }

    private void OnDisable()
    {
        EventManager.StopListening(Events.PreLoadLevel, OnPreLoadLevel);
        EventManager.StopListening(Events.LoadLevel, OnLoadLevel);
    }

    void Start()
    {
        ClearActiveRooms();
        UpdateActiveRooms(DoorTypes.PlayerPortal);
       
    }

    public void RegisterRoom(Room room)
    {
        rooms.Add(room);
    }

    public void UnregisterAllRooms()
    {
        rooms.Clear();
    }

    public bool IsRoomActive(Room room)
    {
        if (activeRooms.Contains(room))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void UpdateCurrentRoom(Room room, bool doAddRoom)
    {
        //add current room to active room
        if (doAddRoom)
        {
            currentRoom = room;
            activeRooms.Add(room);
        }
        else
        {
            if (SpawnDoorManager.Instance.GetCurrentDoorType() == DoorTypes.PlayerPortal)
            {
                activeRooms.Remove(room);
            }
        }

        EventManager.TriggerEvent(Events.UpdateActiveRooms);
    }

    public void UpdateActiveRooms(DoorTypes portalType)
    {
        switch (portalType)
        {
            case DoorTypes.PlayerPortal:
                //if there are more than one active rooms, remove all except current room
                if (activeRooms.Count > 1)
                {
                    activeRooms.Clear();
                    activeRooms.Add(currentRoom);
                }
                break;
            default:
                //add all rooms except current room as active rooms
                for (int i = 0; i < rooms.Count; i++)
                {
                    if (rooms[i] != currentRoom)
                    {
                        activeRooms.Add(rooms[i]);
                    }
                }
                break;
        }

        EventManager.TriggerEvent(Events.UpdateActiveRooms);
    }

    void ClearActiveRooms()
    {
        activeRooms.Clear();
    }

    void OnPreLoadLevel()
    {
        OnAwake();
    }

    void OnLoadLevel()
    {
        Start();
    }
}
