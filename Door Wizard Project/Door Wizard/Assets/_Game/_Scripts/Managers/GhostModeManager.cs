﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostModeManager : Singleton<GhostModeManager>
{
    public bool isGhostModeEnabled = false;

    public GameObject ghostVolume;



    private void OnEnable()
    {
        EventManager.StartListening(Events.PreLoadLevel, OnPreLoadLevel);
    }
    private void OnDisable()
    {
        EventManager.StopListening(Events.PreLoadLevel, OnPreLoadLevel);
    }
    void OnPreLoadLevel()
    {
        isGhostModeEnabled = false;
    }

}
