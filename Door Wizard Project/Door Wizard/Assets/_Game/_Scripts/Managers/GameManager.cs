﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    public bool isPaused = false;
    public bool gameHasStarted = false;
    public bool isDoorOpen = false;
    public bool isTalking = false;
    public bool canInteract = false;
    public bool playerCanMove = true;

    int starsCollected = 0;
    public int StarsCollected
    {
        get { return starsCollected; }
        set { starsCollected = value; }
    }

    public bool Lvl1Complete = false;
    public bool Lvl2Complete = false;


    private void OnEnable()
    {    
       
        starsCollected = PlayerPrefs.GetInt("StarsCollected");
        Debug.Log(starsCollected);
        EventManager.StartListening(Events.Pause, OnPause);
        EventManager.StartListening(Events.ExitPause, ExitPause);
    }

    private void OnDisable()
    {
        EventManager.StopListening(Events.Pause, OnPause);
        EventManager.StopListening(Events.ExitPause, ExitPause);
    }
   
    void Start()
    {
        isPaused = false;
    }

    public void StartGame()
    {
        gameHasStarted = true;
    }

    void OnPause()
    {
        isPaused = true;
    }
    void ExitPause()
    {
        isPaused = false;
    }

    private void OnApplicationQuit()
    {
        if (starsCollected > PlayerPrefs.GetInt("StarsCollected"))
        {
            //saves the amount of starts collected
            PlayerPrefs.SetInt("StarsCollected", starsCollected);
        }

    }

    public void NewGame()
    {
        starsCollected = 0;
        PlayerPrefs.SetInt("StarsCollected", starsCollected);
    }
    public void ContinueGame()
    {
        if (PlayerPrefs.GetInt("StarsCollected") != 0)   
        {
            starsCollected = PlayerPrefs.GetInt("StarsCollected");
        }   

    }
}