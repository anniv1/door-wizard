﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoryProgression : Singleton<StoryProgression>
{
    public List<GameObject> lohkGameObjs;
  
    void SetLohkObjectActive(int currentLohk)
    {
        if (lohkGameObjs != null)
        {
            for (int i = 0; i < lohkGameObjs.Count; i++)
            {
                if (i == currentLohk)
                {
                    lohkGameObjs[i].SetActive(true);
                }
                else
                {
                    lohkGameObjs[i].SetActive(false);
                }
            }
        }
    }

    public void SetLohkActiveInMainMenu(bool setActive)
    {
        if (setActive)
        {
            SetLohkActive();
        }
        else
        {
            if (lohkGameObjs != null)
            {
                for (int i = 0; i < lohkGameObjs.Count; i++)
                {
                    lohkGameObjs[i].SetActive(false);
                }
            }
        }
    }


    void SetLohkActive()
    {
        //if 0 stars collected, set all lohk to false
        if (GameManager.Instance.StarsCollected < 1)
        {
            for (int i = 0; i < lohkGameObjs.Count; i++)
            {
                lohkGameObjs[i].SetActive(false);
            }
        }
        //if 1 star collected, set lokh 0 to true
        else if (GameManager.Instance.StarsCollected == 1)
        {
            SetLohkObjectActive(0);
        }
        //if 2 stars collected, set lokh 1 to true
        else if (GameManager.Instance.StarsCollected < 3)
        {
            SetLohkObjectActive(1);
        }
        //if 5 stars collected, set lokh 2 to true
        else if (GameManager.Instance.StarsCollected == 3)
        {
            SetLohkObjectActive(2);
        }
        //if more than 5 stars collected, set lokh 3 to true
        else if (GameManager.Instance.StarsCollected > 3)
        {
            SetLohkObjectActive(3);
        }
    }
}
