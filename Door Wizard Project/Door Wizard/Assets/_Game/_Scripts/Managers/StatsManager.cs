﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//currently not in use
public class StatsManager : Singleton<StatsManager>
{
    [SerializeField] Slider manaSlider = null;
    public float mana = 0f;
    float initMana;

    protected override void OnAwake()
    {
        initMana = mana;
    }

    private void OnEnable()
    {
        EventManager.StartListening(Events.PreLoadLevel, OnPreLoadLevel);
        EventManager.StartListening(Events.LoadLevel, OnLoadLevel);
    }

    private void OnDisable()
    {
        EventManager.StopListening(Events.PreLoadLevel, OnPreLoadLevel);
        EventManager.StopListening(Events.LoadLevel, OnLoadLevel);
    }

    // Start is called before the first frame update
    void Start()
    {
        if (manaSlider != null)
        {
            manaSlider.value = mana;
        }
    }

    public void AddMana(float amt)
    {
        if (manaSlider != null)
        {
            mana += amt;
            manaSlider.value = mana;
        }
    }

    public bool CheckMana(float manaCost)
    {
        if (mana >= manaCost)
        {
            return true;
        }
        return false;
    }

    void ResetMana()
    {
        mana = initMana;
    }

    void OnPreLoadLevel()
    {
        OnAwake();
    }

    void OnLoadLevel()
    {
        Start();
    }
}
