﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyManager : Singleton<KeyManager>
{
    public class Data
    {
        public KeyTypes currentKeyType;
        public Color currentKeyColor;
    }

    [SerializeField] List<Key> keys = new List<Key>(); //just to visualize the dictionary
    Dictionary<KeyTypes, bool> keysDict = new Dictionary<KeyTypes, bool>();

    protected override void OnAwake()
    {
        UnregisterAllKeys();
    }

    private void OnEnable()
    {
        EventManager.StartListening(Events.LoadLevel, OnLoadLevel);
        EventManager.StartListening(Events.PreLoadLevel, OnPreLoadLevel);
        EventManager.StartListening(Events.GetKey, OnGetKey);
        EventManager.StartListening(Events.GetGhostKey, OnGetGhostKey);
    }

    private void OnDisable()
    {
        EventManager.StopListening(Events.LoadLevel, OnLoadLevel);
        EventManager.StopListening(Events.PreLoadLevel, OnPreLoadLevel);
        EventManager.StopListening(Events.GetKey, OnGetKey);
        EventManager.StopListening(Events.GetGhostKey, OnGetGhostKey);
    }

    void Start()
    {
        ResetKeys();
    }

  
    public void RegisterKey(Key key)
    {
        if (!keysDict.ContainsKey(key.keyType))
        {
            keys.Add(key);
            keysDict.Add(key.keyType, false);
        }
    }

    void UnregisterAllKeys()
    {
        keys.Clear();
        keysDict.Clear();
    }

    void ResetKeys()
    {
        for (int i = 0; i < keys.Count; i++)
        {
            keysDict[keys[i].keyType] = false;
        }
    }

    public bool CheckKey(KeyTypes keyType)
    {
        if (keysDict.ContainsKey(keyType))
        {
            if (keysDict[keyType])
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    void OnPreLoadLevel()
    {
        OnAwake();
    }

    void OnLoadLevel()
    {
        Start();
    }

    void OnGetKey()
    {
        Data data = EventManager.GetTriggerData<Data>();
        if (data != null)
        {
            keysDict[data.currentKeyType] = true;
        }
    } 
    
    void OnGetGhostKey()
    {
        Data data = EventManager.GetTriggerData<Data>();
        if (data != null)
        {
            keysDict[data.currentKeyType] = true;
        }
    }
}
