﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSelectManager : Singleton<LevelSelectManager>
{
    [SerializeField] List<LevelSelectDoor> levelSelectDoors = new List<LevelSelectDoor>();

    private void OnEnable()
    {
        EventManager.StartListening(Events.PreLoadLevel, OnPreLoadLevel);
    }

    private void OnDisable()
    {
        EventManager.StopListening(Events.PreLoadLevel, OnPreLoadLevel);
    }

    public void RegisterLevelSelectDoor(LevelSelectDoor newLevelSelectDoor)
    {
        levelSelectDoors.Add(newLevelSelectDoor);
    }

    void OnPreLoadLevel()
    {
        gameObject.SetActive(true);
    }
}
