﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public Scenes scene = 0;
    public LoopClips levelTheme = 0;
    public Sprite levelLabel = null;
    public Color groundColor = Color.black;
    string closeLoadingScreenDelay = "CloseLoadingScreenDelay";

    private void OnEnable()
    {
        GameManager.Instance.isPaused = false;
        EventManager.StartListening(Events.LoadLevel, OnLoadLevel);
    }

    private void OnDisable()
    {
        EventManager.StopListening(Events.LoadLevel, OnLoadLevel);
    }

    private void Start()
    {
        EventManager.TriggerEvent(Events.RegisterObjects);
        EventManager.TriggerEvent(Events.LoadLevel);
        EventManager.TriggerEvent(Events.GenerateProceduralTerrain);
        EventManager.TriggerEvent(Events.RegisterSpawnedObjects);

        SetActiveDoors();
    }

    void OnLoadLevel()
    {
        AudioManager.Instance.PlayLoopClip(levelTheme);
        MaterialManager.Instance.activeGroundCol = groundColor;

        //if the scene is main menu
        if (scene == Scenes.MainMenu || scene == Scenes.PrizeRoom)
        {
            UIManager.Instance.CloseScreen(UIScreens.GameUI);
            UIManager.Instance.HideHiddenButtons();
            CameraControls.Instance.enabled = false;
            CameraControls.Instance.playMode = false;
            StoryProgression.Instance.SetLohkActiveInMainMenu(true);
        }
        else
        {
            GameManager.Instance.StartGame();
            UIManager.Instance.OpenScreen(UIScreens.GameUI);
            UIManager.Instance.ShowHiddenButtons();
            CameraControls.Instance.enabled = true;
            CameraControls.Instance.playMode = true;
            StoryProgression.Instance.SetLohkActiveInMainMenu(false);
            PuzzleManager.Instance.blueSwitchDown = true;
        }

        StartCoroutine(closeLoadingScreenDelay);
    }

    void SetActiveDoors()
    {
        if (scene == Scenes.Level1)
        {

        }

        if (scene == Scenes.Level2)
        {
            EventManager.TriggerEvent(Events.ObjectDoorActive);
            Debug.Log("obj door active");
        }

        if (scene == Scenes.Tea1)
        {
            EventManager.TriggerEvent(Events.ObjectDoorActive);
            Debug.Log("obj door active");
        }

        if (scene == Scenes.Spooky)
        {
            EventManager.TriggerEvent(Events.GhostDoorActive);
            EventManager.TriggerEvent(Events.ObjectDoorActive);
            Debug.Log("obj door & ghost door active");
        }
    }

    IEnumerator CloseLoadingScreenDelay()
    {
        UIManager.Instance.loadingBar.value = UIManager.Instance.loadingBar.maxValue;

        yield return new WaitForSeconds(0.1f);

        UIManager.Instance.loadingScreen.SetActive(false);

        if (scene != Scenes.MainMenu)
        {
            UIManager.Instance.SetScreenImage(UIScreens.LevelLabel, levelLabel);
        }
    }
}
