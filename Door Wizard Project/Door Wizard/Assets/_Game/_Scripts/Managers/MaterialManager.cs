﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialManager : Singleton<MaterialManager>
{
    public Color activeGroundCol = Color.black;
    public Material disperseMat = null;
    public Material ghostModeMat = null;
}