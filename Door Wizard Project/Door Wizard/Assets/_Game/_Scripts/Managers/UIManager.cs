﻿using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Yarn;
using Yarn.Unity;


public class UIManager : Singleton<UIManager>
{
    [Header("Sprites")]
    public List<Image> keySlots = new List<Image>();
    public GameObject keys = null;
    public Text starsCollectedText;
    public Text ghostKeysCollectedText;
    int keyAmt = 0;
    public int ghostKeyAmt = 0;

    Sprite transparentSprite;
    Sprite keySprite;
    GameObject keySlot;
    List<Image> extraKeyslots = new List<Image>();
    public Scene currentScene;

    [Header("Screens")]
    public List<UIScreen> screenList = new List<UIScreen>();
    List<GameObject> openScreens = new List<GameObject>();
    public List<string> sceneNames = new List<string>();

    [Header("Loading Screen")]
    public Slider loadingBar;
    public GameObject loadingScreen;

    //dictionaries
    Dictionary<UIScreens, GameObject> screenDict = new Dictionary<UIScreens, GameObject>();

    private void OnEnable()
    {
        //events
        EventManager.StartListening(Events.PreLoadLevel, OnPreloadLevel);
        EventManager.StartListening(Events.LoadLevel, OnLoadLevel);
        EventManager.StartListening(Events.GetKey, OnGetKey);
        EventManager.StartListening(Events.GetGhostKey, OnGetGhostKey);
        EventManager.StartListening(Events.GetStar, OnGetStar);
        EventManager.StartListening(Events.Pause, OnPause);
        EventManager.StartListening(Events.ExitPause, ExitPause);

        //cache resources
        transparentSprite = Resources.Load<Sprite>("UI/transparent");
        keySprite = Resources.Load<Sprite>("UI/key");
        keySlot = Resources.Load<GameObject>("UI/key_slot");

        //register screens into dictionary
        for (int i = 0; i < screenList.Count; i++)
        {
            screenDict.Add(screenList[i].screenID, screenList[i].gameObject);
        }

        //set variables
        keySlots = keys.GetComponentsInChildren<Image>().ToList();

        starsCollectedText.text = GameManager.Instance.StarsCollected.ToString();
        ghostKeysCollectedText.text = "0";
        ghostKeyAmt = 0;
    }

    private void OnDisable()
    {
        if (EventManager.Instance != null)
        {
            EventManager.StopListening(Events.PreLoadLevel, OnPreloadLevel);
            EventManager.StopListening(Events.LoadLevel, OnLoadLevel);
            EventManager.StopListening(Events.GetKey, OnGetKey);
            EventManager.StartListening(Events.GetGhostKey, OnGetKey);
            EventManager.StopListening(Events.GetGhostKey, OnGetGhostKey);
            EventManager.StopListening(Events.GetStar, OnGetStar);
            EventManager.StopListening(Events.Pause, OnPause);
            EventManager.StopListening(Events.ExitPause, ExitPause);
  
        }
    }

    void Start()
    {
        ResetKeySprites();
        ResetScreens();
     
        currentScene = SceneManager.GetActiveScene();
        
    }
   

    private void Update()
    {
        if (GameManager.Instance.isPaused == false)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                EventManager.TriggerEvent(Events.Pause);
               
            } 
        }

        else
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                EventManager.TriggerEvent(Events.ExitPause);
            }
        }

    }


    public void OpenScreen(UIScreens screenID)
    {
        screenDict[screenID].SetActive(true);
        openScreens.Add(screenDict[screenID]);
    }
   
    public void CloseScreen(UIScreens screenID)
    {
        screenDict[screenID].SetActive(false);
        openScreens.Remove(screenDict[screenID]);
        CameraControls.Instance.EnableControl(true);

    }

    public void ShowHiddenButtons()
    {
        for (int i = 0; i < screenList.Count; i++)
        {
            screenList[i].ShowGameMenuButtons();
        }
    }

    public void HideHiddenButtons()
    {
        for (int i = 0; i < screenList.Count; i++)
        {
            screenList[i].HideGameMenuButtons();
        }
    }

    public void MoveScreenTo(UIScreens screenID, Vector3 newPos)
    {
        screenDict[screenID].transform.position = newPos;
    }

    public void SetScreenImage(UIScreens screenID, Sprite newSprite)
    {
        AlternatingTitles alternatingTitles = screenDict[screenID].GetComponent<AlternatingTitles>();
        
        if (alternatingTitles != null)
        {
            if (newSprite != null)
            {
                alternatingTitles.image.sprite = newSprite;
                screenDict[screenID].AddComponent<LevelTitleCard>();
                OpenScreen(screenID);
            }
        }
    }

    public bool IsScreenOpen(UIScreens screenID)
    {
        if (screenDict[screenID].activeSelf)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void ResetCamera()
    {
        CameraControls.Instance.ResetCamera();
    }

    void ResetKeySprites()
    {
        for (int i = 0; i < extraKeyslots.Count; i++)
        {
            keySlots.Remove(extraKeyslots[i]);
            Destroy(extraKeyslots[i]);
        }
        
        extraKeyslots.Clear();
        keyAmt = 0;

        for (int i = 0; i < keySlots.Count; i++)
        {
            keySlots[i].sprite = transparentSprite;
        }
    }

    void ResetScreens()
    {
        for (int i = 0; i < openScreens.Count; i++)
        {
            openScreens[i].SetActive(false);
            openScreens.RemoveAt(i);
        }
    }

    void OnPreloadLevel()
    {
        loadingScreen.SetActive(true);
    }

    void OnLoadLevel()
    {
        Start();
    }

    void OnGetKey()
    {
        KeyManager.Data data = EventManager.GetTriggerData<KeyManager.Data>();
        
        if (data != null)
        {
            keyAmt++;

            //if there are more than max keys, add more
            if (keyAmt > keySlots.Count)
            {
                GameObject newKeySlot = Instantiate(keySlot, transform.position, transform.rotation, keys.transform);
                Image newKeySlotImage = newKeySlot.GetComponent<Image>();
                keySlots.Add(newKeySlotImage);
                extraKeyslots.Add(newKeySlotImage);
            }

            //set key sprite color
            keySlots[keyAmt - 1].sprite = keySprite;
            keySlots[keyAmt - 1].color = data.currentKeyColor;
        }
    }

    void OnGetGhostKey()
    {
        ghostKeyAmt++;
        ghostKeysCollectedText.text = ghostKeyAmt.ToString();
    }

    void OnGetStar()
    {
        starsCollectedText.text = GameManager.Instance.StarsCollected.ToString();
    }

    void OnPause()
    {
        GameManager.Instance.isPaused = true;
        OpenScreen(UIScreens.PauseScreen);
        Time.timeScale = 0;
        
    }

    public void ExitPause()
    {
        Time.timeScale = 1;
        CloseScreen(UIScreens.PauseScreen);
        GameManager.Instance.isPaused = false;
    }

    public void LoadScene(string nextLevel)
    {
        StartCoroutine(LoadAsynchronously(nextLevel));
    }

    public void LoadScene(int nextLevel)
    {
        StartCoroutine(LoadAsynchronously(SceneManager.GetSceneByBuildIndex(nextLevel).name));
    }

    //the loading screen code was adapted from: https://www.youtube.com/watch?v=YMj2qPq9CP8&ab_channel=Brackeys
    public IEnumerator LoadAsynchronously(string nextLevel)
    {
        loadingScreen.SetActive(true);
        AsyncOperation operation = SceneManager.LoadSceneAsync(nextLevel);

        if (!SceneManager.GetSceneByName(nextLevel).isLoaded)
        {
            while (!operation.isDone)
            {
                float progress = Mathf.Clamp01(operation.progress);

                loadingBar.value = progress;
                yield return null;
            }
        }
    }
}

public enum UIScreens
{
    None,
    ResetScreen,
    CameraControlScreen,
    PauseScreen,
    OptionsMenuScreen,
    DialogueScreen,
    GameUI,
    ResetProgress,
    ResetProgressSuccess,
    LoadingScreen,
    LevelLabel,
    COUNT
}
