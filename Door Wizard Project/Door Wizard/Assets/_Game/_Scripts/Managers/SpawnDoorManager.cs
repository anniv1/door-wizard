﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnDoorManager : Singleton<SpawnDoorManager>
{
    public DoorTypes currentDoorType = 0;
    public PreviewDoorTypes currentPreviewType = 0;
    public List<GameObject> doors = new List<GameObject>();
    public List<GameObject> previewDoors = new List<GameObject>();
    Dictionary<DoorTypes, GameObject> doorsDict = new Dictionary<DoorTypes, GameObject>(); 
    Dictionary<PreviewDoorTypes, GameObject> prevDoorsDict = new Dictionary<PreviewDoorTypes, GameObject>();

    public bool isPreviewActive = false;

    private void OnEnable()
    {
        EventManager.StartListening(Events.LoadLevel, OnLoadLevel);

        //register doors into dictionary
        for (int i = 0; i < doors.Count; i++)
        {
            Door myDoor = doors[i].GetComponent<Door>();
            if (!doorsDict.ContainsKey(myDoor.doorType))
            {
                doorsDict.Add(myDoor.doorType, doors[i]);
            }
        } 
        
        //register preview doors into dictionary
        for (int i = 0; i < previewDoors.Count; i++)
        {
            PreviewDoor myPreview = previewDoors[i].GetComponent<PreviewDoor>();
            if (!prevDoorsDict.ContainsKey(myPreview.previewDoorType))
            {
                prevDoorsDict.Add(myPreview.previewDoorType, previewDoors[i]);
            }
        }
    }

    private void OnDisable()
    {
        EventManager.StopListening(Events.LoadLevel, OnLoadLevel);
    }

    void Start()
    {
        currentDoorType = DoorTypes.PlayerPortal;
        currentPreviewType = PreviewDoorTypes.PlayerPreview;
    }

    public void ChangeDoorType(DoorTypes newDoorType)
    {
        currentDoorType = newDoorType;

        EventManager.TriggerEvent(Events.ClearPortalDoors);
        AudioManager.Instance.PlaySFXClip(SFXClips.ChangeDoor);

        //update active rooms
        RoomManager.Instance.UpdateActiveRooms(currentDoorType);
    }
    
    public void ChangePreviewType(PreviewDoorTypes newPreviewType)
    {
        currentPreviewType = newPreviewType;
        EventManager.TriggerEvent(Events.ChangePreviewType);
    }

    public DoorTypes GetCurrentDoorType()
    {
        return currentDoorType;
    } 
    
    public PreviewDoorTypes GetCurrentPreviewType()
    {
        return currentPreviewType;
    }

    public GameObject GetCurrentDoor()
    {
        return doorsDict[currentDoorType];
    } 
    
    public GameObject GetCurrentPreview()
    {
        return prevDoorsDict[currentPreviewType];
    }

    void OnLoadLevel()
    {
        Start();
    }
}
