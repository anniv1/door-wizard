﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PuzzleManager : Singleton<PuzzleManager>
{

    [SerializeField] RedZone finalBarrier = null;
    [SerializeField] Key finalKey = null;
    public bool blueSwitchDown = true;


    public List<Puzzle> puzzles = new List<Puzzle>();
    public List<Renderer> glowSphereRends = new List<Renderer>();
    Dictionary<Puzzle, bool> puzzleChecks = new Dictionary<Puzzle, bool>();

    protected override void OnAwake()
    {
        UnregisterAllPuzzles();
    }

    private void OnEnable()
    {
        EventManager.StartListening(Events.PreLoadLevel, OnPreLoadLevel);
        EventManager.StartListening(Events.LoadLevel, OnLoadLevel);
    }

    private void OnDisable()
    {
        EventManager.StopListening(Events.PreLoadLevel, OnPreLoadLevel);
        EventManager.StopListening(Events.LoadLevel, OnLoadLevel);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.R))
        {
            UIManager.Instance.OpenScreen(UIScreens.ResetScreen);
        }
    }

    public void RegisterPuzzle(Puzzle newPuzzle)
    {
        puzzles.Add(newPuzzle);
        puzzleChecks.Add(newPuzzle, newPuzzle.puzzleComplete);
    }

    public void RegisterFinalKey(Key newFinalKey)
    {
        finalKey = newFinalKey;
    }

    public void RegisterFinalBarrier(RedZone newFinalBarrier)
    {
        finalBarrier = newFinalBarrier;
    }

    public void RegisterGlowSphereRend(Renderer newGlowSpehereRend)
    {
        glowSphereRends.Add(newGlowSpehereRend);
    }

    public Renderer GetGlowSphereRend()
    {
        if (glowSphereRends.Count > 0)
        {
            int randomGlowSphereRend = Random.Range(0, glowSphereRends.Count);
            return glowSphereRends[randomGlowSphereRend];
        }

        return null;
    }

    private void Start()
    {
        ResetPuzzles();
    }

    void UnregisterAllPuzzles()
    {
        puzzles.Clear();
        puzzleChecks.Clear();
    }

    void ResetPuzzles()
    {
        for (int i = 0; i < puzzles.Count; i++)
        {
            puzzleChecks[puzzles[i]] = false;
        }
    }

    public void CheckPuzzle()
    {
        //if any of the puzzles are not completed, break function
        for (int i = 0; i < puzzles.Count; i++)
        {
            if (puzzles[i].puzzleComplete == false)
            {
                return;
            }
        }

        //else, mark complete
        OnAllPuzzlesComplete();
    }

    void OnAllPuzzlesComplete()
    {
        if (finalKey != null)
        {
            finalKey.gameObject.SetActive(true);
            ParticlesManager.Instance.InstantiateParticle(Particles.CompleteMiniPuzzle, finalKey.transform.position, finalKey.transform);
        }

        if (finalBarrier != null)
        {
            finalBarrier.gameObject.SetActive(false);
            ParticlesManager.Instance.InstantiateParticle(Particles.CompleteMiniPuzzle, finalBarrier.transform.position, finalBarrier.transform);
        }

    }

    void OnPreLoadLevel()
    {
        OnAwake();
    }

    void OnLoadLevel()
    {
        Start();
    }
}