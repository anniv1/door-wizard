﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticlesManager : Singleton<ParticlesManager>
{

    [SerializeField] List<GameObject> particles = null;
    [SerializeField] List<Particles> particleKeys = null;

    Dictionary<Particles, GameObject> particlesDict = new Dictionary<Particles, GameObject>();
    void Start()
    {
        //initialize particles in dictionary
        for (int i = 0; i < particleKeys.Count; i++)
        {
            particlesDict.Add(particleKeys[i], particles[i]);
        }
    }


    public void InstantiateParticle(Particles particle, Vector3 spawnPos)
    {
        if (particles.Count > 0)
        {
            Instantiate(particlesDict[particle], spawnPos, Quaternion.identity);
        }
    }

    public void InstantiateParticle(Particles particle, Vector3 spawnPos, Transform parent)
    {
        if (particles.Count > 0)
        {
            Instantiate(particlesDict[particle], spawnPos, Quaternion.identity, parent);
        }
    }

    public void InstantiateParticle(Particles particle, Vector3 spawnPos, Quaternion rot, Transform parent)
    {
        if (particles.Count > 0)
        {
            Instantiate(particlesDict[particle], spawnPos, rot, parent);
        }
    }
}



public enum Particles
{
    PlaceDoor,
    CantPlaceDoor,
    GetKey,
    UnlockGate,
    CompleteMiniPuzzle,
    UnlockDoor,
    GateAmbience,
    CloseDoor,
    Poof,
    Lohk,
    COUNT
}
