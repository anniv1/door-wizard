﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Events
{
    NONE = -1,
    CustomTriggerEvent,
    GameLoaded,
    GameFinished,
    SaveStarted,
    //--------
    PreLoadLevel,
    RegisterObjects,
    LoadLevel,
    GenerateProceduralTerrain,
    RegisterSpawnedObjects,
    //
    Pause,
    ExitPause,
    RunDialogue,
    StartConversation,
    DontStartConversation,
    ObjectDoorActive,
    GhostDoorActive,
    ModifyCameraFollow,
    ResetCameraFollow,
    //
    PlayerMove,
    PlayerKnockback,
    EnterRoom,
    ClearPortalDoors,
    ColourSwitch,
    UnlockDoor,
    GetGhostKey,
    GetKey,
    GetStar,
    UpdateActiveRooms,
    ResetPositions,
    //
    EnableGhostMode,
    DisableGhostMode,
    //
    ChangePreviewType,
    WaterSwitch,
    DetachedGlowSphereSpawned,
    //
    TOTAL
}