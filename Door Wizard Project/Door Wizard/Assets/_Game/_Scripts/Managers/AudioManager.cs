﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : Singleton<AudioManager>
{
    [SerializeField] AudioSource loopSource = null;
    [SerializeField] AudioSource sfxSource = null;

    [Header("Loop Clips")]
    [SerializeField] List<LoopClips> loopClipKeys = null;
    [SerializeField] List<AudioClip> loopClipValues = null;

    [Header("SFX Clips")]
    [SerializeField] List<SFXClips> SFXClipKeys = null;
    [SerializeField] List<AudioClip> SFXClipValues = null;

    Dictionary<LoopClips, AudioClip> loopClips = new Dictionary<LoopClips, AudioClip>();
    Dictionary<SFXClips, AudioClip> SFXClips = new Dictionary<SFXClips, AudioClip>();

    float initLoopVolume;

    private void OnEnable()
    {
        //initialize loop clips in dictionary
        for (int i = 0; i < loopClipKeys.Count; i++)
        {
            loopClips.Add(loopClipKeys[i], loopClipValues[i]);
        }

        //initialist sfx clips in dictionary
        for (int i = 0; i < SFXClipKeys.Count; i++)
        {
            SFXClips.Add(SFXClipKeys[i], SFXClipValues[i]);
        }

        initLoopVolume = loopSource.volume;
    }

    public void PlayLoopClip(LoopClips clip)
    {
        if (loopSource != null)
        {
            loopSource.clip = loopClips[clip];
            loopSource.Play();
        }
    }

    public void PlaySFXClip(SFXClips clip)
    {
        if (sfxSource != null)
        {
            sfxSource.clip = SFXClips[clip];
            sfxSource.Play();

        }
    }

    public void StopClip(AudioSource audio)
    {
        audio.Stop();
    }

    public void ChangeLoopVolume(float newVolume)
    {
        ChangeVolume(loopSource, newVolume);
    }

    public void ChangeSFXVolume(float newVolume)
    {
        ChangeVolume(sfxSource, newVolume);
    }

    void ChangeVolume(AudioSource audio, float newVolume)
    {
        if (audio != null)
        {
            audio.volume = newVolume;
        }
    }

    void OnPauseGame()
    {
        ChangeLoopVolume(loopSource.volume - 0.3f); //decrease volume
    }

    void OnUnpauseGame()
    {
        ChangeLoopVolume(initLoopVolume); //reset volume
    }
}

public enum LoopClips
{
    MainMenu,
    MainTheme,
    PrizeRoom,
    TeaRealm,
    SpookyRealm,
    // realm themes
    COUNT
}

public enum SFXClips
{
    OpenDoor,
    CloseDoor,
    LockedDoor,
    UnlockDoor,
    LightSwitched,
    ColorSwitched,
    WeightSwitched,
    PushBlock,
    PuzzleComplete,
    CantPlaceDoor,
    GetKey,
    UIPopUp,
    ChangeDoor,
    Teleport,
    PlaceDoor,
    Water,
    COUNT
}
