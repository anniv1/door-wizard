﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalDoor : OpenCloseDoor
{
    [Header("Final Door")]
    public GameObject doorPortal;

    void Start()
    {
        doorPortal.SetActive(false);
    }

    public override void OnUnlockDoor()
    {
        if (KeyManager.Instance.CheckKey(keyType))
        {
            isLocked = false;
            doorPortal.SetActive(true);
            ParticlesManager.Instance.InstantiateParticle(Particles.UnlockGate, doorPortal.transform.position, doorPortal.transform.rotation, transform);
            ParticlesManager.Instance.InstantiateParticle(Particles.GateAmbience, doorPortal.transform.position, doorPortal.transform.rotation, transform);
        }        
    }

    protected override void OnRegisterObjects()
    {
        DoorManager.Instance.RegisterFinalDoor(this);
    }
}
