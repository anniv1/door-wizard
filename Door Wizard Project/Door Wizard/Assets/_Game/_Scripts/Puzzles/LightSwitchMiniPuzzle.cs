﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightSwitchMiniPuzzle : MiniPuzzle
{
    [Header("Light Switch Mini Puzzle")]
    [SerializeField] GameObject lever = null;
    [SerializeField] GameObject instructions;
    
    public Renderer glowBoxRend;
    public Renderer glowSphereRend;
    public Material normalMat;
    public Material glowMat;

    Vector3 switchRot = new Vector3(-105f, 0f, 0f);

    bool playerDetected = false;

    DetachedGlowSphere detachedGlowSphere = null;
    ObjectSparkles sparkles;
    private void OnEnable()
    {
        EventManager.StartListening(Events.DetachedGlowSphereSpawned, OnDetachedGlowSphereSpawned);
        OnDetachedGlowSphereSpawned();
        sparkles = GetComponent<ObjectSparkles>();
    }

    private void OnDisable()
    {
        EventManager.StopListening(Events.DetachedGlowSphereSpawned, OnDetachedGlowSphereSpawned);
    }

    private void Update()
    {
        if (playerDetected)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                sparkles.OnObjectInteraction();
                AudioManager.Instance.PlaySFXClip(SFXClips.LightSwitched);

                if (miniPuzzleComplete == false)
                {
                    Debug.Log("switch on");
                    lever.transform.Rotate(switchRot);
                    OnMiniPuzzleComplete();

                    glowBoxRend.material = glowMat;
                    glowSphereRend.material = glowMat;

                    if (detachedGlowSphere != null)
                    {
                        detachedGlowSphere.OnMiniPuzzleComplete();
                    }
                }
                else
                {
                    Debug.Log("switch off");
                    lever.transform.Rotate(-switchRot);
                    miniPuzzleComplete = false;

                    glowBoxRend.material = normalMat;
                    glowSphereRend.material = normalMat;
                }
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.layer == LayerManager.Instance.PlayerLayerInt || other.gameObject.layer == LayerManager.Instance.GhostModeLayerInt)
        {
            playerDetected = true;
            instructions.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other)

    {
        if (other.gameObject.layer == LayerManager.Instance.PlayerLayerInt || other.gameObject.layer == LayerManager.Instance.GhostModeLayerInt)
        {
            playerDetected = false;
            instructions.SetActive(false);
        }
    }

    void OnDetachedGlowSphereSpawned()
    {
        if (glowSphereRend == null)
        {
            glowSphereRend = PuzzleManager.Instance.GetGlowSphereRend();

            if (glowSphereRend != null)
            {
                detachedGlowSphere = glowSphereRend.GetComponent<DetachedGlowSphere>();
            }
        }
    }
}
