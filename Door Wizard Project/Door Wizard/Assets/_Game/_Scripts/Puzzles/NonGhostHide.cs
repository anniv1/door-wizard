﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NonGhostHide : MonoBehaviour
{
    [SerializeField] List<BoxCollider> nonGhostCollider;


    private void OnEnable()
    {
        
        for (int i = 0; i < nonGhostCollider.Count; i++)
        {
            nonGhostCollider[i].enabled = true;
        }

        EventManager.StartListening(Events.EnableGhostMode, OnEnableGhostMode);
        EventManager.StartListening(Events.DisableGhostMode, OnDisableGhostMode);
    }

    private void OnDisable()
    {
        EventManager.StopListening(Events.EnableGhostMode, OnEnableGhostMode);
        EventManager.StopListening(Events.DisableGhostMode, OnDisableGhostMode);
    }


    void OnEnableGhostMode()
    {
        for (int i = 0; i < nonGhostCollider.Count; i++)
        {
            nonGhostCollider[i].enabled = false;
        }
    }

    void OnDisableGhostMode()
    {
        for (int i = 0; i < nonGhostCollider.Count; i++)
        {
            nonGhostCollider[i].enabled = true;
        }
    }
}
