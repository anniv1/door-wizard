﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnappingWeightSwitch : MiniPuzzle
{
    [SerializeField] string snapLayer = "Weight Switch";
    [SerializeField] string ghostSnapLayer = "GhostPushBlock";
    public Transform snapPos = null;
    protected float t = 0;
    protected bool doSnap = false;

    public Material snapMat = null;

    protected Renderer rend;
    protected Collider col;
    protected GameObject otherObject = null;
    protected int snapLayerInt = 0;
    protected int ghostSnapLayerInt = 0;

    private void OnEnable()
    {
        rend = GetComponent<Renderer>();
        col = GetComponent<Collider>();
        snapLayerInt = LayerMask.NameToLayer(snapLayer);
        ghostSnapLayerInt = LayerMask.NameToLayer(ghostSnapLayer);
    }

    protected virtual void OnObjectEnter()
    {
        //start timer
        t = 0;
        doSnap = true;
    }

    protected virtual void OnObjectStay()
    {
        //if the correct piece stays in trigger for long enough
        if (t < 0.5f)
        {
            t += Time.deltaTime;
        }
        else
        {
            //snap to designated position & disable movement
            otherObject.transform.position = snapPos.position;
            otherObject.transform.parent = transform;
            Rigidbody snapRbody = otherObject.GetComponent<Rigidbody>();
            snapRbody.constraints = RigidbodyConstraints.FreezeAll;
            snapRbody.isKinematic = true;

            //mark complete
            AudioManager.Instance.PlaySFXClip(SFXClips.WeightSwitched);
            otherObject.GetComponent<ObjectSparkles>().StopSparkles();
            col.enabled = false;
            rend.material = snapMat;
            OnMiniPuzzleComplete();
            doSnap = false;
        }
    }

    protected virtual void OnObjectExit()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == snapLayerInt || other.gameObject.layer == ghostSnapLayerInt)
        {
            otherObject = other.gameObject;

            OnObjectEnter();
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (doSnap == true)
        {
            if (other.gameObject.layer == snapLayerInt || other.gameObject.layer == ghostSnapLayerInt)
            {
                OnObjectStay();
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == snapLayerInt || other.gameObject.layer == ghostSnapLayerInt)
        {
            OnObjectExit();

            //if piece exits trigger before timer is complete
            t = 0;
            otherObject = null;
            doSnap = false;
        }
    }
}
