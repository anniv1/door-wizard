﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class HiddenPuzzle : Puzzle
{
    [Header("Hidden Puzzle")]
    [SerializeField] bool spawn = false;
    [SerializeField] GameObject starKey = null;

    void Start()
    {
        starKey.SetActive(false);

        if (spawn)
        {
            if (Random.value < 0.7f)
            {
                gameObject.SetActive(false);
            }
        }
    }
    protected override void OnPuzzleComplete()
    {
        base.OnPuzzleComplete();

        starKey.SetActive(true);
    }

    protected override void OnRegisterSpawnedObjects()
    {
        RegisterMiniPuzzles();
        RegisterPushBlocks();
    }
}
