﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeaPuzzle : Puzzle
{
    [Header("Tea Puzzle")]
    [SerializeField] bool hiddenSwitch = false;
    [SerializeField] WaterSwitch waterSwitch = null;

    void Start()
    {
        if (hiddenSwitch)
        {
            waterSwitch.gameObject.SetActive(false);
        }
        else
        {
            waterSwitch.gameObject.SetActive(true);
        }
    }

    protected override void OnPuzzleComplete()
    {
        base.OnPuzzleComplete();

        waterSwitch.gameObject.SetActive(true);
        ParticlesManager.Instance.InstantiateParticle(Particles.CompleteMiniPuzzle, waterSwitch.transform.position, waterSwitch.transform);
    }
}
