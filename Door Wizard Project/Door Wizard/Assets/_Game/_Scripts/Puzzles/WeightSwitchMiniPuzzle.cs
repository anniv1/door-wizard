﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeightSwitchMiniPuzzle : MiniPuzzle
{
    [Header("Weight Switch Mini Puzzle")]
    public string weightLayer = "";
    int weightLayerInt;
    public bool isPlayerSwitch; //true: player activated. false: cube/item activated

    [Header("/Material Change")]
    Renderer switchRend;
    public Material newSwitchCol;

    protected override void Awake()
    {
        base.Awake();

        switchRend = GetComponent<Renderer>();
    }

    private void Start()
    {
        weightLayerInt = LayerMask.NameToLayer(weightLayer);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (isPlayerSwitch == true)
        {
            if (other.gameObject.layer == LayerManager.Instance.PlayerLayerInt || other.gameObject.layer == LayerManager.Instance.GhostModeLayerInt)
            {   
                AudioManager.Instance.PlaySFXClip(SFXClips.WeightSwitched);

                switchRend.material = newSwitchCol;
                OnMiniPuzzleComplete();
                enabled = false;
            }
        }

        else
        {
            if (!miniPuzzleComplete)
            {
                if (other.gameObject.layer == weightLayerInt || other.gameObject.layer == LayerManager.Instance.GhostModeLayerInt) //change layerMask accordingly
                {
                    AudioManager.Instance.PlaySFXClip(SFXClips.WeightSwitched);

                    switchRend.material = newSwitchCol;
                    OnMiniPuzzleComplete();
                    enabled = false;
                }
            }
        }
    }
}
