﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GhostHide : MonoBehaviour
{
    [SerializeField] GameObject ghostKeyobj;

    List<Renderer> ghostRends = new List<Renderer>();
    Material ghostInitMat;  

    private void OnEnable()
    {
        ghostKeyobj.SetActive(true);

        EventManager.StartListening(Events.EnableGhostMode, OnEnableGhostMode);
        EventManager.StartListening(Events.DisableGhostMode, OnDisableGhostMode);
    }

    private void OnDisable()
    {
        EventManager.StopListening(Events.EnableGhostMode, OnEnableGhostMode);
        EventManager.StopListening(Events.DisableGhostMode, OnDisableGhostMode);
    }

    private void Start()
    { 
        ghostKeyobj.SetActive(false);
        ghostRends = ghostKeyobj.GetComponentsInChildren<Renderer>().ToList();
        ghostInitMat = ghostRends[0].material;

       
    }

    void OnEnableGhostMode()
    {
        ghostKeyobj.SetActive(true);

        for (int i = 0; i < ghostRends.Count; i++)
        {
            if (!gameObject.CompareTag("Ghost"))
            {
                ghostRends[i].material = MaterialManager.Instance.ghostModeMat;
            }
            
        }
    }

    void OnDisableGhostMode()
    {
        ghostKeyobj.SetActive(false);

        for (int i = 0; i < ghostRends.Count; i++)
        {
            ghostRends[i].material = ghostInitMat;
        }
    }
}
