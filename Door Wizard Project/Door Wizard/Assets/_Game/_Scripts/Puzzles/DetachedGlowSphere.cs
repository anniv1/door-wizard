﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetachedGlowSphere : MiniPuzzle
{
    private void OnEnable()
    {
        OnRegisterObjects();
    }
    void OnRegisterObjects()
    {
        Renderer rend = GetComponent<Renderer>();
        PuzzleManager.Instance.RegisterGlowSphereRend(rend);

        EventManager.TriggerEvent(Events.DetachedGlowSphereSpawned);
    }
}
