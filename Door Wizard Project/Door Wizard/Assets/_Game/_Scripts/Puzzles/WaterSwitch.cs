﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterSwitch : MonoBehaviour
{
    [SerializeField] GameObject instructions;
    public GameObject switchObj;
    public Vector3 switchDown = Vector3.up * 0.5f;
    bool isSwitchDown;

    bool playerDetected;

    private void OnEnable()
    {
       
        isSwitchDown = false;
    }

    private void OnDisable()
    {
        if (EventManager.Instance != null)
        {
    
        }
    }

    private void Update()
    {
        if (playerDetected)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Debug.Log("player triggered");

                //water levels come down
                EventManager.TriggerEvent(Events.WaterSwitch);
                
                switchObj.transform.position -= switchDown;
                AudioManager.Instance.PlaySFXClip(SFXClips.Water);
                enabled = false;
                isSwitchDown = true;
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.layer == LayerManager.Instance.PlayerLayerInt && isSwitchDown == false)
        {
            playerDetected = true;
            instructions.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == LayerManager.Instance.PlayerLayerInt && isSwitchDown == false)
        {
            playerDetected = false;
            instructions.SetActive(false);
        }
    }

}
