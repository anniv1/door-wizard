﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Puzzle : MonoBehaviour
{
    public bool puzzleComplete;
    public bool completeAny = false;
    public List<MiniPuzzle> miniPuzzles = new List<MiniPuzzle>();
    [SerializeField] List<PushBlock> pushBlocks = new List<PushBlock>();
    List<Transform> pushBlockTransforms = new List<Transform>();

    private void OnEnable()
    {
        EventManager.StartListening(Events.RegisterSpawnedObjects, OnRegisterSpawnedObjects);
    }

    private void OnDisable()
    {
        EventManager.StopListening(Events.RegisterSpawnedObjects, OnRegisterSpawnedObjects);
    }

    private void OnTriggerExit(Collider other)
    {
        for (int i = 0; i < pushBlocks.Count; i++)
        {
            pushBlocks[i].transform.position = pushBlockTransforms[i].position;
        }
    }

    public void RegisterMiniPuzzle(MiniPuzzle newMiniPuzzle)
    {
        miniPuzzles.Add(newMiniPuzzle);
    }

    public void CheckMiniPuzzle()
    {
        if (completeAny)
        {
            OnPuzzleComplete();
        }

        else
        {
            //if any of the puzzles are not completed, return function
            for (int i = 0; i < miniPuzzles.Count; i++)
            {
                if (miniPuzzles[i].miniPuzzleComplete == false)
                {
                    return;
                }
            }

            //else, mark complete
            OnPuzzleComplete();
        }
    }

    protected virtual void OnPuzzleComplete()
    {
        AudioManager.Instance.PlaySFXClip(SFXClips.PuzzleComplete);
        puzzleComplete = true;
        PuzzleManager.Instance.CheckPuzzle();
        enabled = false;
    }

    protected virtual void OnRegisterSpawnedObjects()
    {
        PuzzleManager.Instance.RegisterPuzzle(this);
        RegisterMiniPuzzles();
        RegisterPushBlocks();
    }

    protected void RegisterMiniPuzzles()
    {
        miniPuzzles = GetComponentsInChildren<MiniPuzzle>().ToList();
        List<MiniPuzzle> miniPuzzlesToRemove = new List<MiniPuzzle>();

        for (int i = 0; i < miniPuzzles.Count; i++)
        {
            miniPuzzles[i].puzzle = this;
            
            if (miniPuzzles[i].skipRegisterObject)
            {
                miniPuzzlesToRemove.Add(miniPuzzles[i]);
            }
        }

        for (int i = 0; i < miniPuzzlesToRemove.Count; i++)
        {
            miniPuzzles.Remove(miniPuzzlesToRemove[i]);
        }
    }

    protected void RegisterPushBlocks()
    {
        pushBlocks = GetComponentsInChildren<PushBlock>().ToList();

        for (int i = 0; i < pushBlocks.Count; i++)
        {
            pushBlockTransforms.Add(pushBlocks[i].GetComponent<Transform>());
        }
    }
}
