﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrierPuzzle : Puzzle
{
    [Header("Barrier Puzzle")]
    public GameObject barrier = null;

    protected override void OnPuzzleComplete()
    {
        base.OnPuzzleComplete();
        barrier.SetActive(false);
        Debug.Log("barrier down");
        ParticlesManager.Instance.InstantiateParticle(Particles.CompleteMiniPuzzle, barrier.transform.position, transform);
    }
}
