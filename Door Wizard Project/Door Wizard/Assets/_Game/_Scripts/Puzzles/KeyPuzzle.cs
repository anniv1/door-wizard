﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyPuzzle : Puzzle
{
    [Header("Barrier Puzzle")]
    public Key key = null;

    protected override void OnPuzzleComplete()
    {
        base.OnPuzzleComplete();
        key.keyBubble.SetActive(false);
        ParticlesManager.Instance.InstantiateParticle(Particles.CompleteMiniPuzzle, key.transform.position, key.transform);
    }
}
