﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedZone : MonoBehaviour
{
    public bool isFinalBarrier;
    public bool linkedToColour; //change this in the inspector 
    GameObject barrier;

    private void OnEnable()
    {
        EventManager.StartListening(Events.ColourSwitch, OnColorSwitch);
        EventManager.StartListening(Events.RegisterObjects, OnRegisterObjects);
    }

    private void OnDisable()
    {
        if (EventManager.Instance != null)
        {
            EventManager.StopListening(Events.ColourSwitch, OnColorSwitch);
            EventManager.StopListening(Events.RegisterObjects, OnRegisterObjects);
        }
    }
    private void Start()
    {
        gameObject.SetActive(true);
        barrier = transform.GetChild(0).gameObject;

        if (linkedToColour)
        {
            if (PuzzleManager.Instance.blueSwitchDown == true)
            {
                barrier.SetActive(false);
            }
            else
            {
                barrier.SetActive(true);
            }
        }
    }

    void OnColorSwitch()
    {
        if (linkedToColour == false)
        {
            barrier.SetActive(true);
        }

        if (linkedToColour == true) //this is for the barriers connected to the colour switches
        {
            if (PuzzleManager.Instance.blueSwitchDown == true)
            {
                barrier.SetActive(false);
            }
            else
            {
                barrier.SetActive(true);
            }
        }
    }

    void OnRegisterObjects()
    {
        if (isFinalBarrier)
        {
            PuzzleManager.Instance.RegisterFinalBarrier(this);
        }
    }
}
