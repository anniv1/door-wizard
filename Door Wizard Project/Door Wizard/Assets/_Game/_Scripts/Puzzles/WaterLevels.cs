﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterLevels : MonoBehaviour
{
    public List<float> waterDownPerSegment = new List<float>();
    public float waterDownSpeed = 0.5f;
    public GameObject waterSurface = null;
    public GameObject waterSphere = null;

    List<float> waterDown = new List<float>();
    float totalWaterDown = 0f;
    string fillAmount = "_FillAmount";
    Vector3 newSurfacePos;
    Vector3 newSurfaceScale;
    float newFillAmount;
    Renderer waterSphereRend;
    int waterDownModifier = 1;
    bool doChangeWaterLevel;
    float t = 0;

    private void OnEnable()
    {
        EventManager.StartListening(Events.LoadLevel, OnLoadLevel);
        EventManager.StartListening(Events.WaterSwitch, OnWaterSwitch);
    }

    private void OnDisable()
    {
        EventManager.StopListening(Events.LoadLevel, OnLoadLevel);
        EventManager.StopListening(Events.WaterSwitch, OnWaterSwitch);
    }

    void Start()
    {
        waterSphereRend = waterSphere.GetComponent<Renderer>();
    }

    void Update()
    {
        if (doChangeWaterLevel)
        {
            //drop water level by lerp
            ChangeWaterLevel();
        }
    }

    void ChangeWaterLevel()
    {
        if (t < 1f)
        {
            t += Time.deltaTime * waterDownSpeed;
            waterSurface.transform.position = Vector3.Lerp(waterSurface.transform.position, newSurfacePos, t);
            waterSphereRend.material.SetFloat(fillAmount, Mathf.Lerp(waterSphereRend.material.GetFloat(fillAmount), newFillAmount, t));
        }
        else
        {
            doChangeWaterLevel = false;
            t = 0;
        }
    }

    void OnLoadLevel()
    {
        //adjust water down levels according to portal count in each segment
        for (int i = 0; i < waterDownPerSegment.Count; i++)
        {
            //find number of branches (and to that effect, portals) in each segment
            int segmentPortalCount = SegmentManager.Instance.segments[i].segmentBranches.Count;

            //if there is more than one, split the water down levels accordingly
            if (segmentPortalCount > 1)
            {
                float newWaterDown = waterDownPerSegment[i] / segmentPortalCount;
                for (int j = 0; j < segmentPortalCount; j++)
                {
                    waterDown.Add(newWaterDown);
                    totalWaterDown += newWaterDown;
                }
            }
            //else, just add the water down whole
            else
            {
                waterDown.Add(waterDownPerSegment[i]);
                totalWaterDown += waterDownPerSegment[i];
            }
        }

        //set surface scale multiplier
        //surfaceScaleModifier /= totalWaterDown;
    }

    void OnWaterSwitch()
    {
   
        //set new water level
        newSurfacePos = waterSurface.transform.position - (Vector3.up * waterDown[0]);
        newFillAmount = waterSphereRend.material.GetFloat(fillAmount) + (waterDown[0] - waterDownModifier);

        waterDown.RemoveAt(0);
        t = 0;
        doChangeWaterLevel = true;

        if (waterDown.Count == 0)
        {
            waterSurface.gameObject.SetActive(false);
        }
    }
}
