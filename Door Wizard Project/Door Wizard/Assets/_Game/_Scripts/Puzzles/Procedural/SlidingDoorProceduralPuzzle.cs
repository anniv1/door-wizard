﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SlidingDoorProceduralPuzzle : ProceduralPuzzle
{
    [Header("Sliding Door Procedural Puzzle")]
    [SerializeField] SlidingDoor finalSlidingDoor = null;
    [SerializeField] List<SlidingDoor> slidingDoors = new List<SlidingDoor>();

    void RandomizeDoors()
    {
        List<SlidingDoor> slidingDoorsInUse = new List<SlidingDoor>();

        //assign a random exit for every door
        for (int i = 0; i < slidingDoors.Count; i++)
        {
            //temporarily store all sliding door options
            List<SlidingDoor> slidingDoorOptions = new List<SlidingDoor>();
            for (int j = 0; j < slidingDoors.Count; j++)
            {
                slidingDoorOptions.Add(slidingDoors[j]);
            }

            slidingDoorOptions.Remove(slidingDoors[i]);

            //assign exit portal 1
            slidingDoors[i].exitPortal1 = slidingDoorOptions[Random.Range(0, slidingDoorOptions.Count)];
            slidingDoorsInUse.Add(slidingDoors[i].exitPortal1);
            slidingDoorOptions.Remove(slidingDoors[i].exitPortal1);

            //assign exit portal 2
            slidingDoors[i].exitPortal2 = slidingDoorOptions[Random.Range(0, slidingDoorOptions.Count)];
            slidingDoorsInUse.Add(slidingDoors[i].exitPortal2);
            slidingDoorOptions.Remove(slidingDoors[i].exitPortal2);
        }

        //if none of the doors connect to the final water switch door, force a connection
        if (!slidingDoorsInUse.Contains(finalSlidingDoor))
        {
            //get random door to forcefully connect with
            SlidingDoor forceConnectedDoor = slidingDoors[Random.Range(0, slidingDoors.Count)];

            //get one of the two sliding door exits and assign one of them to the final door
            int randomExit = Random.Range(0, 2);
            switch (randomExit)
            {
                case 0:
                    forceConnectedDoor.exitPortal1 = finalSlidingDoor;
                    break;
                case 1:
                    forceConnectedDoor.exitPortal2 = finalSlidingDoor;
                    break;
                default:
                    break;
            }
        }
    }

    protected override void OnGenerateProceduralPuzzle()
    {
        base.OnGenerateProceduralPuzzle();

        if (randomize)
        {
            RandomizeDoors();
        }
    }

    protected override void OnRegisterObjects()
    {
        base.OnRegisterObjects();

        slidingDoors = GetComponentsInChildren<SlidingDoor>().ToList();
    }
}
