﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ShogiProceduralPuzzle : ProceduralPuzzle
{
    public List<ShogiWeightSwitch> switches = new List<ShogiWeightSwitch>();
    public List<ShogiPushBlock> pieces = new List<ShogiPushBlock>();

    Vector3 northRot = new Vector3(0f, 180f, 0f);
    Vector3 southRot = Vector3.zero;

    void RandomizePuzzle()
    {
        List<ShogiWeightSwitch> tempSwitches = new List<ShogiWeightSwitch>();
        List<ShogiPushBlock> tempPieces = new List<ShogiPushBlock>();

        for (int i = 0; i < switches.Count; i++)
        {
            tempSwitches.Add(switches[i]);
            tempPieces.Add(pieces[i]);
        }

        for (int i = 0; i < switches.Count; i++)
        {
            //get random direction
            Shogi.Direction newDirection = (Shogi.Direction)Random.Range(0, (int)Shogi.Direction.COUNT);

            //find random switch, assign new direction
            int randomSwitch = Random.Range(0, tempSwitches.Count);
            SetNewDirection(tempSwitches[randomSwitch], newDirection);
            tempSwitches.RemoveAt(randomSwitch);

            //find random piece, assign the same direction
            int randomPiece = Random.Range(0, tempPieces.Count);
            SetNewDirection(tempPieces[randomPiece], newDirection);
            tempPieces.RemoveAt(randomPiece);
        }
    }

    void SetNewDirection(ShogiPushBlock currentPiece, Shogi.Direction currentDirection)
    {
        currentPiece.direction = currentDirection;
        SetDirection(currentPiece.direction, currentPiece.transform);
    }

    void SetNewDirection(ShogiWeightSwitch currentSwitch, Shogi.Direction currentDirection)
    {
        currentSwitch.direction = currentDirection;
        SetDirection(currentSwitch.direction, currentSwitch.transform);
    }

    void SetCurrentDirection(ShogiPushBlock currentPiece)
    {
        SetDirection(currentPiece.direction, currentPiece.transform);
    }

    void SetCurrentDirection(ShogiWeightSwitch currentSwitch)
    {
        SetDirection(currentSwitch.direction, currentSwitch.transform);
    }

    void SetDirection(Shogi.Direction dir, Transform transform)
    {
        switch (dir)
        {
            case Shogi.Direction.North:
                transform.localRotation = Quaternion.Euler(northRot);
                break;
            case Shogi.Direction.South:
                transform.localRotation = Quaternion.Euler(southRot);
                break;
        }
    }

    public void CheckSwitch()
    {
        //if not all switches are complete, return
        for (int i = 0; i < switches.Count; i++)
        {
            if (switches[i].miniPuzzleComplete == false)
            {
                return;
            }
        }

        //else, mark puzzle as complete
        OnMiniPuzzleComplete();
        AudioManager.Instance.PlaySFXClip(SFXClips.PuzzleComplete);
    }

    protected override void OnGenerateProceduralPuzzle()
    {
        base.OnGenerateProceduralPuzzle();

        for (int i = 0; i < switches.Count; i++)
        {
            switches[i].shogiPuzzle = this;
        }

        if (randomize)
        {
            RandomizePuzzle();
        }
        else
        {
            for (int i = 0; i < switches.Count; i++)
            {
                SetCurrentDirection(switches[i]);
                SetCurrentDirection(pieces[i]);
            }
        }
    }

    protected override void OnRegisterObjects()
    {
        base.OnRegisterObjects();

        switches = GetComponentsInChildren<ShogiWeightSwitch>().ToList();
        pieces = GetComponentsInChildren<ShogiPushBlock>().ToList();
    }
}
