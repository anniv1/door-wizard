﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProceduralPuzzle : MiniPuzzle
{
    public bool randomize = false;

    protected void OnEnable()
    {
        OnRegisterObjects();
        OnGenerateProceduralPuzzle();
    }

    protected virtual void OnGenerateProceduralPuzzle()
    {
        
    }

    protected virtual void OnRegisterObjects()
    {

    }
}
