﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShogiPushBlock : MonoBehaviour
{
    public Shogi.Direction direction = 0;

    Rigidbody rbody;

    private void OnEnable()
    {
        rbody = GetComponent<Rigidbody>();
    }

    public void OnSnapToSwitch()
    {
        rbody.isKinematic = true;
        rbody.constraints = RigidbodyConstraints.FreezeAll;
    }
}
