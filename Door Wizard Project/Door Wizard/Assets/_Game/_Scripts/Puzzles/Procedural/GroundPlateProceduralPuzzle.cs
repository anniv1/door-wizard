﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundPlateProceduralPuzzle : ProceduralPuzzle
{
    [Header("Ground Plate Procedural Puzzle")]
    [SerializeField] GameObject groundPlate = null;
    [SerializeField] Vector2Int spawnGridSize = new Vector2Int(3, 3);
    [SerializeField] Vector2Int spawnAmountRange = new Vector2Int(4, 5);
    [SerializeField] float spawnOffset = 6.75f;
    [SerializeField] List<SnappingWeightSwitch> groundPlates = new List<SnappingWeightSwitch>();

    void GeneratePuzzle()
    {
        List<int> platesToSpawn = new List<int>();
        int spawnAmount = Random.Range(spawnAmountRange.x, spawnAmountRange.y);
        
        for (int i = 0; i < spawnAmount; i++)
        {
            int randomGridPos = Random.Range(0, spawnGridSize.x * spawnGridSize.y);
            if (!platesToSpawn.Contains(randomGridPos))
            {
                platesToSpawn.Add(randomGridPos);
            }
            else
            {
                while (platesToSpawn.Contains(randomGridPos))
                {
                    randomGridPos = Random.Range(0, spawnGridSize.x * spawnGridSize.y);
                }

                platesToSpawn.Add(randomGridPos);
            }
        }

        for (int j = 0; j < spawnGridSize.y; j++)
        {
            for (int i = 0; i < spawnGridSize.x; i++)
            {
                int spawnedPlate = spawnGridSize.y * j + i;

                if (platesToSpawn.Contains(spawnedPlate))
                {
                    //spawn plate object
                    Vector3 spawnPos = new Vector3(transform.position.x + i * spawnOffset, transform.position.y, transform.position.z + j * spawnOffset);
                    GameObject newGroundPlate = Instantiate(groundPlate, spawnPos, Quaternion.identity, transform);

                    //store snap switch
                    SnappingWeightSwitch newSnapSwitch = newGroundPlate.GetComponent<SnappingWeightSwitch>();
                    groundPlates.Add(newSnapSwitch);
                }
            }
        }

        transform.localRotation = Quaternion.Euler(Vector3.zero);
    }

    protected override void OnGenerateProceduralPuzzle()
    {
        base.OnGenerateProceduralPuzzle();

        if (randomize)
        {
            GeneratePuzzle();
        }
    }
}
