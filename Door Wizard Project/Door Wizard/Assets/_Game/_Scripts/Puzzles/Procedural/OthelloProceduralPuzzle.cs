﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class OthelloProceduralPuzzle : ProceduralPuzzle
{
    [Header("Othello Procedural Puzzle")]
    [SerializeField] int maxAttempts = 1;
    [SerializeField] Othello.Color finalColor = Othello.Color.White;
    [SerializeField] GameObject floor = null;
    [SerializeField] List<OthelloPiece> pieces = new List<OthelloPiece>();
    public List<OthelloPiece> blackPieces = new List<OthelloPiece>();
    public List<OthelloPiece> whitePieces = new List<OthelloPiece>();
    int attemptsLeft = 0;

    [SerializeField] bool doSpawnPieces = false;
    [SerializeField] Vector2Int boardSize = new Vector2Int(8, 8);
    [SerializeField] GameObject othelloPiece = null;
    [SerializeField] float spawnOffset = 1f;
    [SerializeField] float spawnRotOffset = -45f;

    // Shaking
    bool doShake = false;
    Vector3 initPos;
    float shakeAmplitude = 20f;
    float shakeDuration = 0.2f;

    protected override void Awake()
    {
        initPos = transform.position;
    }

    void SpawnPieces()
    {
        for (int j = 0; j < boardSize.y; j++)
        {
            for (int i = 0; i < boardSize.x; i++)
            {
                //set spawn position
                Vector3 spawnPosition = new Vector3();
                spawnPosition.x = transform.position.x + i * spawnOffset;
                spawnPosition.y = transform.position.y;
                spawnPosition.z = transform.position.z - j * spawnOffset;

                //register piece
                OthelloPiece newPiece = Instantiate(othelloPiece, spawnPosition, Quaternion.identity).GetComponent<OthelloPiece>();
                pieces.Add(newPiece);
                newPiece.transform.parent = transform;
                
                //set variables
                newPiece.othelloPuzzle = this;
                newPiece.SetColor(true);
                newPiece.coords.x = i + 1;
                newPiece.coords.y = j + 1;

                //store in lists
                if (newPiece.color == Othello.Color.Black)
                {
                    blackPieces.Add(newPiece);
                }
                else
                {
                    whitePieces.Add(newPiece);
                }
            }
        }

        //rotate to angle
        transform.localRotation = Quaternion.Euler(new Vector3(transform.rotation.x, spawnRotOffset, transform.rotation.z));
    }

    public void CheckPiece(OthelloPiece currentPiece)
    {
        CheckAdjacentPieces(currentPiece);

        if (doSpawnPieces)
        {
            if (!(whitePieces.Count != 0 ^ blackPieces.Count != 0))
            {
                return;
            }
        }
        else
        {
            if (finalColor == Othello.Color.Black)
            {
                if (whitePieces.Count != 0)
                {
                    return;
                }
            }
            else
            {
                if (blackPieces.Count != 0)
                {
                    return;
                }
            }
        }

        //mark puzzle as complete
        OnMiniPuzzleComplete();
        AudioManager.Instance.PlaySFXClip(SFXClips.PuzzleComplete);

        for (int i = 0; i < pieces.Count; i++)
        {
            pieces[i].OnPuzzleComplete();
        }
    }

    public void CheckAdjacentPieces(OthelloPiece currentPiece)
    {
        //check index of adjacent pieces
        CheckPieceIndex(currentPiece.coords.y - 1, Directions.Up);                                                                  // 🡡
        CheckPieceIndex(Mathf.Min(currentPiece.coords.y - 1, boardSize.x - currentPiece.coords.x), Directions.UpRight);             // 🡥
        CheckPieceIndex(boardSize.x - currentPiece.coords.x, Directions.Right);                                                     // 🡢
        CheckPieceIndex(Mathf.Min(boardSize.x - currentPiece.coords.x, boardSize.y - currentPiece.coords.y), Directions.DownRight); // 🡦
        CheckPieceIndex(boardSize.y - currentPiece.coords.y, Directions.Down);                                                      // 🡣
        CheckPieceIndex(Mathf.Min(boardSize.y - currentPiece.coords.y, currentPiece.coords.x - 1), Directions.DownLeft);            // 🡧
        CheckPieceIndex(currentPiece.coords.x - 1, Directions.Left);                                                                // 🡠
        CheckPieceIndex(Mathf.Min(currentPiece.coords.x - 1, currentPiece.coords.y - 1), Directions.UpLeft);                        // 🡤

        void CheckPieceIndex(int range, Directions dir)
        {

            List<OthelloPiece> adjacentPieces = new List<OthelloPiece>();
            int currentPieceIndex = pieces.FindIndex(x => x == currentPiece);

            for (int i = 1; i <= range; i++)
            {
                //get index of adjacent piece
                int adjacentPieceIndex = currentPieceIndex;
                switch (dir)
                {
                    case Directions.Up:
                        adjacentPieceIndex -= i * boardSize.x;
                        break;
                    case Directions.UpRight:
                        adjacentPieceIndex -= i * boardSize.x;
                        adjacentPieceIndex += i;
                        break;
                    case Directions.Right:
                        adjacentPieceIndex += i;
                        break;
                    case Directions.DownRight:
                        adjacentPieceIndex += i * boardSize.x + i;
                        break;
                    case Directions.Down:
                        adjacentPieceIndex += i * boardSize.x;
                        break;
                    case Directions.DownLeft:
                        adjacentPieceIndex += i * boardSize.x - i;
                        break;
                    case Directions.Left:
                        adjacentPieceIndex -= i;
                        break;
                    case Directions.UpLeft:
                        adjacentPieceIndex -= i * boardSize.x;
                        adjacentPieceIndex -= i;
                        break;
                }

                //if adjacent piece is a different color, add to list
                if (pieces[adjacentPieceIndex].color != currentPiece.color)
                {
                    adjacentPieces.Add(pieces[adjacentPieceIndex]);
                }
                //else if adjacent piece is the same color and not directly adjacent to current piece,
                //change color of all adjacent pieces
                else if (i > 1 && pieces[adjacentPieceIndex].color == currentPiece.color)
                {
                    for (int j = 0; j < adjacentPieces.Count; j++)
                    {
                        adjacentPieces[j].color = currentPiece.color;
                        adjacentPieces[j].SetColor(false);

                        if (currentPiece.color == Othello.Color.Black)
                        {
                            whitePieces.Remove(adjacentPieces[j]);
                            blackPieces.Add(adjacentPieces[j]);
                        }
                        else
                        {
                            blackPieces.Remove(adjacentPieces[j]);
                            whitePieces.Add(adjacentPieces[j]);
                        }
                    }

                    return;
                }
                else
                {
                    return;
                }
            }
        }
    }

    #region Shake()
    protected void FixedUpdate()
    {
        if (doShake == true)
        {
            Vector3 newPos = initPos + Random.insideUnitSphere * (Time.deltaTime * shakeAmplitude);
            newPos.y = transform.position.y;
            newPos.z = transform.position.z;

            transform.position = newPos;
        }
    }

    protected void Shake()
    {
        StartCoroutine("Shaking");
    }

    protected IEnumerator Shaking()
    {
        if (doShake == false)
        {
            doShake = true;
        }

        yield return new WaitForSeconds(shakeDuration);

        doShake = false;
        transform.position = initPos;

        StopAllCoroutines();
    }
    #endregion

    protected override void OnGenerateProceduralPuzzle()
    {
        base.OnGenerateProceduralPuzzle();

        if (doSpawnPieces)
        {
            SpawnPieces();
        }
        else
        {
            pieces = GetComponentsInChildren<OthelloPiece>().ToList();

            //set variables of exsting pieces
            for (int i = 0; i < pieces.Count; i++)
            {
                pieces[i].othelloPuzzle = this;
                pieces[i].coords.x = (i % boardSize.x) + 1;
                pieces[i].coords.y = ((int)Mathf.Floor(i / boardSize.x)) + 1;
                if (randomize)
                {
                    pieces[i].SetColor(true);
                    finalColor = (Othello.Color)Random.Range((int)Othello.Color.Black, (int)Othello.Color.COUNT);
                    if (pieces[i].color == Othello.Color.Black)
                    {
                        blackPieces.Add(pieces[i]);
                    }
                    else
                    {
                        whitePieces.Add(pieces[i]);
                    }
                }
                else
                {
                    pieces[i].SetColor(false);
                }
            }
        }

        if (maxAttempts > 0)
        {
            attemptsLeft = maxAttempts;
        }

        if (floor != null)
        {
            if (finalColor == Othello.Color.Black)
            {
                floor.transform.localRotation = Quaternion.Euler(Vector3.zero);
            }
            else
            {
                floor.transform.localRotation = Quaternion.Euler(new Vector3(0f, 0f, 180f));
            }
        }
    }
}
