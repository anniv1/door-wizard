﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniPuzzle : MonoBehaviour
{
    public bool miniPuzzleComplete = false;
    public bool skipRegisterObject = false;
    [HideInInspector] public Puzzle puzzle;


    protected virtual void Awake()
    {
      
    }


    public virtual void OnMiniPuzzleComplete()
    {
        miniPuzzleComplete = true;
        puzzle.CheckMiniPuzzle();
    }
}
