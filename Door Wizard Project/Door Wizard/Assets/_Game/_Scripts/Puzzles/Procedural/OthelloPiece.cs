﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OthelloPiece : MonoBehaviour
{
    public Othello.Color color = 0;
    public Vector2Int coords = Vector2Int.zero;

    [HideInInspector] public OthelloProceduralPuzzle othelloPuzzle;
    //Renderer rend;
    Collider col;
    bool doFlip = true;
    Othello.Color initColor;

    bool playerDetected = false;
    ObjectSparkles sparkles;

    Vector3 blackRot = Vector3.zero;
    Vector3 whiteRot = new Vector3(0f, 0f, 180f);

    private void OnEnable()
    {
    
        sparkles = GetComponent<ObjectSparkles>();
        col = GetComponent<Collider>();
        initColor = color;
    }

    private void Update()
    {
        if (playerDetected)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                switch (color)
                {
                    case Othello.Color.Black:
                        transform.localRotation = Quaternion.Euler(whiteRot);
                        color = Othello.Color.White;
                        othelloPuzzle.blackPieces.Remove(this);
                        othelloPuzzle.whitePieces.Add(this);
                        break;
                    case Othello.Color.White:
                        transform.localRotation = Quaternion.Euler(blackRot);
                        color = Othello.Color.Black;
                        othelloPuzzle.whitePieces.Remove(this);
                        othelloPuzzle.blackPieces.Add(this);
                        break;
                    default:
                        break;
                }

                AudioManager.Instance.PlaySFXClip(SFXClips.ColorSwitched);
                sparkles.OnObjectInteraction();
                othelloPuzzle.CheckPiece(this);
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (doFlip)
        {
            if (other.gameObject.layer == LayerManager.Instance.PlayerLayerInt)
            {
                playerDetected = true;
            }    
        }
    }

    private void OnTriggerExit(Collider other)
    {
        playerDetected = false;
    }

    public void SetColor(bool isRandom)
    {
        if (isRandom)
        {
            color = (Othello.Color)Random.Range((int)Othello.Color.Black, (int)Othello.Color.COUNT);
        }

        switch (color)
        {
            case Othello.Color.Black:
                transform.localRotation = Quaternion.Euler(blackRot);
                break;
            case Othello.Color.White:
                transform.localRotation = Quaternion.Euler(whiteRot);
                break;
            default:
                break;
        }
    }

    public void OnPuzzleComplete()
    {
        enabled = false;
        col.enabled = false;
    }

    public void OnResetPiece()
    {
        color = initColor;
        SetColor(false);
    }

    protected IEnumerator TeleportDelay()
    {
        doFlip = false;

        yield return new WaitForSeconds(0.5f);

        doFlip = true;
    }
}
