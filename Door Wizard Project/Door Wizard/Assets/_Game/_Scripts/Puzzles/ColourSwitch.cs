﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ColourSwitch : MonoBehaviour
{
    [SerializeField] GameObject instructions = null;
    public GameObject redSwitch = null;
    public GameObject blueSwitch = null;
    Vector3 switchUp = Vector3.up * 0.5f;

    bool playerDetected = false;
    ObjectSparkles sparkles;

    private void OnEnable()
    {
        EventManager.StartListening(Events.ColourSwitch, OnColorSwitch);
        sparkles = GetComponent<ObjectSparkles>();
    }

    private void OnDisable()
    {
        if (EventManager.Instance != null)
        {
            EventManager.StopListening(Events.ColourSwitch, OnColorSwitch);
        }
    }

    void Start()
    {
        SetStartingColor();
    }

    private void Update()
    {
        if (playerDetected)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                //change switch condition check
                if (PuzzleManager.Instance.blueSwitchDown)
                {
                    PuzzleManager.Instance.blueSwitchDown = false;
                }
                else
                {

                    PuzzleManager.Instance.blueSwitchDown = true;
                }

                //switch all switches and doors in scene
                EventManager.TriggerEvent(Events.ColourSwitch);
                sparkles.OnObjectInteraction();
            }
        }
    }

    void SetStartingColor()
    {
        if (PuzzleManager.Instance.blueSwitchDown)
        {
            redSwitch.transform.position += switchUp;
        }
        else
        {
            blueSwitch.transform.position += switchUp;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.layer == LayerManager.Instance.PlayerLayerInt || other.gameObject.layer == LayerManager.Instance.GhostModeLayerInt)
        {
            playerDetected = true;
            instructions.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == LayerManager.Instance.PlayerLayerInt || other.gameObject.layer == LayerManager.Instance.GhostModeLayerInt)
        {
            playerDetected = false;
            instructions.SetActive(false);
        }
    }

    void OnColorSwitch()
    {
        AudioManager.Instance.PlaySFXClip(SFXClips.ColorSwitched);
        
        if (PuzzleManager.Instance.blueSwitchDown == true)
        {
            blueSwitch.transform.position -= switchUp;
            redSwitch.transform.position += switchUp;
        }
        else
        {
           
            blueSwitch.transform.position += switchUp;
            redSwitch.transform.position -= switchUp;
        }
    }
}