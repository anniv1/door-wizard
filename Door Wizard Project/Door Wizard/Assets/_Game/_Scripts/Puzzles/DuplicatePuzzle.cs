﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DuplicatePuzzle : MonoBehaviour
{
    public DuplicatePuzzles duplicateID = 0;
}

public enum DuplicatePuzzles
{
    None,
    TEA4_Othello,
    COUNT
}
