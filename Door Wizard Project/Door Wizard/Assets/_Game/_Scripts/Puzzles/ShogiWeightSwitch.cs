﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShogiWeightSwitch : SnappingWeightSwitch
{
    public Shogi.Direction direction = 0;
    ShogiPushBlock currentPiece = null;

    [HideInInspector] public ShogiProceduralPuzzle shogiPuzzle = null;

    protected override void OnObjectEnter()
    {
        //if piece direction matches the weight switch direction
        currentPiece = otherObject.GetComponent<ShogiPushBlock>();
        if (currentPiece.direction == direction)
        {
            //start timer
            t = 0;
            doSnap = true;
        }
    }

    protected override void OnObjectStay()
    {
        if (otherObject == currentPiece.gameObject)
        {
            //if the correct piece stays in trigger for long enough
            if (t < 1f)
            {
                t += Time.deltaTime;
            }
            else
            {
                //snap to designated position & disable movement
                otherObject.transform.position = snapPos.position;
                otherObject.transform.parent = transform;
                col.enabled = false;
                currentPiece.OnSnapToSwitch();
                currentPiece = null;

                //mark complete
                miniPuzzleComplete = true;
                shogiPuzzle.CheckSwitch();

                doSnap = false;
            }
        }
    }

    protected override void OnObjectExit()
    {

    }
}
