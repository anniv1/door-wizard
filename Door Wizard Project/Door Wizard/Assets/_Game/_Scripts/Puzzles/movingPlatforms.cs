﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movingPlatforms : MonoBehaviour
{
    public Transform pointA;
    public Transform pointB;
    public AnimationCurve animCurve;
    public float timeToMove;
    float time;

    public GameObject player;

    void Update()
    {
        time += Time.deltaTime;


        transform.position = Vector3.Lerp(pointA.position, pointB.position, animCurve.Evaluate(time / timeToMove));
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            player.transform.SetParent(transform); //sets the player as a child of the platform
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            player.transform.SetParent(null); //sets the player as a child of the platform
        }
    }
}
