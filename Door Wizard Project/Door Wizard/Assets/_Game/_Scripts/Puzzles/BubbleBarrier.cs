﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BubbleBarrier : MonoBehaviour
{
    [SerializeField] bool flippedDir = false;
    [SerializeField] float knockbackForce = 15f;
    public class Data
    {
        public Vector3 currentKnockBackDir;
        public float currentKnockbackForce;
    }

    ParticleSystem bubbleParticles;
    List<ParticleCollisionEvent> collisionEvents = new List<ParticleCollisionEvent>();
    Vector3 knockBackDir = Vector3.zero;

    private void OnEnable()
    {
        bubbleParticles = GetComponent<ParticleSystem>();
        knockBackDir = -transform.forward.normalized;
        if (flippedDir)
        {
            knockBackDir *= -1;
        }
    }

    private void OnParticleCollision(GameObject other)
    {
        int numCollisionEvents = bubbleParticles.GetCollisionEvents(other, collisionEvents);

        if (other.gameObject.layer == LayerManager.Instance.PlayerLayerInt)
        {
            Data data = new Data
            {
                currentKnockBackDir = knockBackDir,
                currentKnockbackForce = knockbackForce
            };

            for (int i = 0; i < numCollisionEvents; i++)
            {
                EventManager.TriggerEvent(Events.PlayerKnockback, data);
            }
        }
    }
}
