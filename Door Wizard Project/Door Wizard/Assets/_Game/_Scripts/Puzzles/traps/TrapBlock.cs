﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.HighDefinition;

public class TrapBlock : MonoBehaviour
{
    string baseColor = "_BaseColor";
    int stepCount = 0;
    int maxStepCount;
    Renderer rend;
    DecalProjector decalProjector;

    [HideInInspector] public TrapFloor trapFloor = null;

    private void OnEnable()
    {
        rend = GetComponentInChildren<Renderer>();
        decalProjector = GetComponentInChildren<DecalProjector>();

    }

    public void OnObjectWasRegistered()
    {
        maxStepCount = trapFloor.stepCracks.Count;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerManager.Instance.PlayerLayerInt || other.gameObject.layer == LayerManager.Instance.GhostModeLayerInt)
        {
            stepCount ++;

            if (stepCount < maxStepCount)
            {
                decalProjector.material = trapFloor.stepCracks[stepCount];
                Debug.Log(stepCount);
            }
            else
            {
                rend.material.SetColor("_BaseColor", trapFloor.holeColor);
                other.transform.position = trapFloor.connectingBasement.position;
            }

            if (stepCount == maxStepCount || stepCount > maxStepCount)
            {
                gameObject.SetActive(false);
            }
        }
    }
}
