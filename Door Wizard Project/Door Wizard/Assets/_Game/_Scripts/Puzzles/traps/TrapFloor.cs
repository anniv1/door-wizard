﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TrapFloor : MonoBehaviour
{
    public Transform connectingBasement = null;
    public List<Material> stepCracks = new List<Material>();
    public Color holeColor = Color.black;
    public List<TrapBlock> trapBlocks = new List<TrapBlock>();

    private void OnEnable()
    {
        trapBlocks = GetComponentsInChildren<TrapBlock>().ToList();

        for (int i = 0; i < trapBlocks.Count; i++)
        {
            trapBlocks[i].trapFloor = this;
            trapBlocks[i].OnObjectWasRegistered();
        }
    }
 

}
