﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractTrap : MonoBehaviour
{
    public GameObject instructions;
    public Transform connectingBasement = null;

    private void Start()
    {
        instructions.SetActive(false);
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.layer == LayerManager.Instance.PlayerLayerInt)
        {
            instructions.SetActive(true);

            if (Input.GetKeyDown(KeyCode.Space))
            {
                other.transform.position = connectingBasement.position;
                Debug.Log("trap successful");
            }


        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == LayerManager.Instance.PlayerLayerInt)
        {
            instructions.SetActive(false);
        }
    }
}
