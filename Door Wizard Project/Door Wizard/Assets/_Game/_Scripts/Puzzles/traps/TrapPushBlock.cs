﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapPushBlock : MonoBehaviour
{
    Rigidbody rbody;
    [HideInInspector] public TrapFloor trapFloor = null;
    public Transform connectingBasement = null;
    public List<Color> stepColors = new List<Color>();

    public PushBlock pushBlock;
    private void Awake()
    {
        rbody = GetComponent<Rigidbody>();
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.layer == LayerManager.Instance.PlayerLayerInt)
        {
            collision.transform.position = connectingBasement.position;
        }
    }
   
}
