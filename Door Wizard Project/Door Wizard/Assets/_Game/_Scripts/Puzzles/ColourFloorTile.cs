﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColourFloorTile : MonoBehaviour
{
    public bool isBlue;
    public bool isRed;
    public List<GameObject> tiles;

    private void OnEnable()
    {
        EventManager.StartListening(Events.ColourSwitch, OnColorSwitch);
        
        for (int i = 0; i < tiles.Count; i++)
        {
            if (PuzzleManager.Instance.blueSwitchDown == true)
            {
                if (isBlue == true)
                {
                    tiles[i].SetActive(true);
                }
                else
                {
                    tiles[i].SetActive(false);
                }
            }
            else
            {
                if (isRed == true)
                {
                    tiles[i].SetActive(true);
                }
                else
                {
                    tiles[i].SetActive(false);
                }
            }
        }

    }

    private void OnDisable()
    {
        if (EventManager.Instance != null)
        {
            EventManager.StopListening(Events.ColourSwitch, OnColorSwitch);
        }
    }
    
    void OnColorSwitch()
    {
        for (int i = 0; i < tiles.Count; i++)
        {
            if (PuzzleManager.Instance.blueSwitchDown == true)
            {
                if (isBlue == true)
                {
                    tiles[i].SetActive(true);
                }
                else
                {
                    tiles[i].SetActive(false);
                }
            }
            else
            {
                if (isRed == true)
                {
                    tiles[i].SetActive(true);
                }
                else
                {
                    tiles[i].SetActive(false);
                }
            }
        }
    }
}
