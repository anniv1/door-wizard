﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class PushBlock : MonoBehaviour
{
    Rigidbody rbody;
    public bool isGhostBlock = false;
    public BoxCollider col = null;
    public BoxCollider otherCol = null;
    Vector3 initPos;

    private void Awake()
    {
        rbody = GetComponent<Rigidbody>();
        initPos = transform.position;
    }
    private void Update()
    {
        if (transform.position.y < -10f)
        {
            OnResetPositions();
        }

        if (GhostModeManager.Instance.isGhostModeEnabled == true && isGhostBlock == false)
        {
            col.enabled = false;
            otherCol.enabled = false;
            rbody.useGravity = false;
        }
        else
        {
            col.enabled = true;
            otherCol.enabled = true;
            rbody.useGravity = true;
        }
      
    }

    public void OnSnapToSwitch()
    {
        rbody.isKinematic = true;
        rbody.constraints = RigidbodyConstraints.FreezeAll;
    }

    void OnResetPositions()
    {
        transform.position = initPos;
    }


}
