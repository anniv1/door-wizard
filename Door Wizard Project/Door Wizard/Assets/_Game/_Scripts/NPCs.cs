﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Yarn;
using Yarn.Unity;

public class NPCs : MonoBehaviour
{
    [SerializeField] GameObject instructions;
    public Animator anim = null;

    public bool isLohk;

    bool playerDetected = false;
    bool dialogueIsRunning = false;
    public string talkToNode = "";
    public DialogueRunner dialogueRunner;

    [Header("Optional")]
    public YarnProgram scriptToLoad;

    ObjectSparkles sparkles;

    private void OnEnable()
    {
        EventManager.StartListening(Events.RegisterObjects, OnRegisterDialogueRunner);

        sparkles = GetComponent<ObjectSparkles>();
    }
    private void OnDisable()
    {
        EventManager.StopListening(Events.RegisterObjects, OnRegisterDialogueRunner);
      
    }

    void Start()
    {
        dialogueRunner.Add(scriptToLoad);
    }

    private void Update()
    {
       
        if (playerDetected && !dialogueIsRunning)
        {
            if (Input.GetKeyDown(KeyCode.Space)|| GameManager.Instance.isTalking == true)
            {
                Debug.Log("CONVO STARTED");
                OnStartConversation();
            }
        }
    }

    void OnStartConversation()
    {
        anim.SetBool("isTalking", true);
        dialogueIsRunning = true;
        instructions.SetActive(false);
        EventManager.TriggerEvent(Events.RunDialogue);
        GameManager.Instance.isTalking = false;
        RunDialogue();
    }

    void RunDialogue()
    {
        dialogueRunner.StartDialogue(talkToNode);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerManager.Instance.PlayerLayerInt || other.gameObject.layer == LayerManager.Instance.GhostModeLayerInt)
        {
            if (!dialogueIsRunning)
            {
                instructions.SetActive(true);
                playerDetected = true;
                GameManager.Instance.canInteract = true;
                
                anim.SetBool("isWaving", true);

            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == LayerManager.Instance.PlayerLayerInt || other.gameObject.layer == LayerManager.Instance.GhostModeLayerInt)
        {
            instructions.SetActive(false);
            playerDetected = false;
            GameManager.Instance.canInteract = false;

            anim.SetBool("isWaving", false);
            anim.SetBool("isTalking", false);
        }
    }

    void OnRegisterDialogueRunner()
    {
       dialogueRunner = FindObjectOfType<DialogueRunner>();
    }

}
