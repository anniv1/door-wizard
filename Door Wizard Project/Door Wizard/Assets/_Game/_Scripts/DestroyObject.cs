﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyObject : MonoBehaviour
{
    [SerializeField] float destroyDelay = 0.5f;

    private void OnEnable()
    {
        Destroy(gameObject, destroyDelay);
    }
}
