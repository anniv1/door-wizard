﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectibleStarKey : MonoBehaviour
{
    float value = 1f;
    Collider starCollider;

    void Start()
    {
        starCollider = GetComponent<Collider>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == LayerManager.Instance.PlayerLayerInt)
        {
            StatsManager.Instance.AddMana(value);
            starCollider.enabled = false;

            //adds to star reset
            GameManager.Instance.StarsCollected = GameManager.Instance.StarsCollected + 1;
            PlayerPrefs.SetInt("starsCollected", GameManager.Instance.StarsCollected);

            EventManager.TriggerEvent(Events.GetStar);
            AudioManager.Instance.PlaySFXClip(SFXClips.GetKey);
            ParticlesManager.Instance.InstantiateParticle(Particles.PlaceDoor, transform.position);
            Destroy(gameObject, 0.001f);
        }
        
    }
}
